/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        white: "#FFFFFF",
        darkBlue: "#100B26",
        sandyBrown: "#EFA00B",
        darkGrey: "#383838",
        standardViolet: "#9283EF",
        darkViolet: "#3D348B",
        lightViolet: "#E9EBF8",
        lightGrey: "#E8E8E8",
      },
      fontFamily: {
        poppins: ["Poppins"],
      },
      width: {
        128: "40rem",
      },
    },
  },
  plugins: [
    require("tailwindcss-inner-border"),
    require("tailwind-scrollbar"),
    require("tailwindcss-animate"),
  ],
};
