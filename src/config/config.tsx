export const API_URL = "https://deskbooking.dev.webundsoehne.com";

export const getLoginToken = () => {
  return localStorage.getItem("loginToken");
};

export const getRefreshToken = () => {
  return localStorage.getItem("refreshToken");
};
export const setLoginToken = (loginToken: string) => {
  return localStorage.setItem("loginToken", loginToken);
};

export const setRefreshToken = (refreshToken: string) => {
  return localStorage.setItem("refreshToken", refreshToken);
};
