import { Link } from "react-router-dom";

export const Footer = () => {
  return (
    <div className="bg-lightViolet flex flex-col justify-center items-center gap-5 p-3 bottom-0 left-0 right-0">
      <div className="flex flex-row gap-10 items-center">
        <Link to={`/admin/gdpr-page`} className="label">
          Datenschutz
        </Link>
        <Link to={`/admin/impressum`} className="label">
          Impressum
        </Link>
      </div>
      <p className="text-center">
        © Doneus & Reichl Booking Solutions / Coding School am Wörthersee
      </p>
    </div>
  );
};
