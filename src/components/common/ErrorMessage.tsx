import { ErrorMessageProps } from "../../types/commonType";

export const ErrorMessage = ({ message }: ErrorMessageProps) => {
  return <p className="text-red-600">{message}</p>;
};
