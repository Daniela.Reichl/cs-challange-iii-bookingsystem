import { Link } from "react-router-dom";
import { useGetLoggedInUser } from "../../hooks/get/useGetLoggedInUser";
import { Loader } from "./Loader";
import { NavbarProps } from "../../types/commonType";

export const Navbar = ({ showLinks, onClickLink }: NavbarProps) => {
  const { data: loggedInUser, isLoading } = useGetLoggedInUser();

  // logout the user
  const handleLogout = () => {
    localStorage.removeItem("loginToken");
    localStorage.removeItem("refreshToken");
    onClickLink();
    window.location.href = "/";
  };

  // showing the navbar
  if (!showLinks || isLoading) {
    <div>
      <Loader />
    </div>;
    return null;
  }

  return (
    <div
      className={`flex flex-col label absolute right-12 top-24 ${
        showLinks ? "openNavbar" : ""
      }`}
    >
      <Link to={`/home`} onClick={onClickLink} className="navbar rounded-t-md ">
        Home
      </Link>
      <Link
        to={`/user/profile/${loggedInUser.id}`}
        className="navbar "
        onClick={onClickLink}
      >
        Profil
      </Link>
      <Link
        to={`/book/office-overview/`}
        className="navbar"
        state={{ linkTags: "/book/office-bookingplan" }}
        onClick={onClickLink}
      >
        Tisch buchen
      </Link>
      <Link
        to={`/user/bookings/${loggedInUser.id}`}
        className="navbar"
        onClick={onClickLink}
      >
        Buchungen
      </Link>

      <Link
        to={`/favourites/${loggedInUser.id}`}
        className="navbar"
        onClick={onClickLink}
      >
        Favoriten
      </Link>

      {loggedInUser?.isAdmin && (
        <Link to={`/admin`} className="navbar" onClick={onClickLink}>
          Administrator
        </Link>
      )}
      <Link
        to={`/`}
        className=" bg-lightViolet hover:bg-darkViolet p-5 hover:text-lightViolet transition-all ease-in-out; rounded-b-md"
        onClick={handleLogout}
      >
        Logout
      </Link>
    </div>
  );
};
