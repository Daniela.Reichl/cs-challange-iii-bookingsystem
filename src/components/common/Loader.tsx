export const Loader = () => {
  return (
    <div className="flex justify-center items-center">
      <div className="animate-spin border-4 border-lightGrey border-t-4 border-t-standardViolet rounded-full w-12 h-12"></div>
    </div>
  );
};
