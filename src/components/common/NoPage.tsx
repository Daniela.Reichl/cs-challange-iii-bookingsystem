import { BackButton } from "./BackButton";
import error from "/src/assets/icons/error.svg";

export const NoPage = () => {
  return (
    <div className="baseMain">
      <div className="baseContent">
        <img className="h-60" src={error} alt="error" />
        <h1>404</h1>
        <h2>Page not found</h2>
        <p>
          The Page you are looking for doesn't exist or an other error occurred.
        </p>
      </div>
      <BackButton />
    </div>
  );
};
