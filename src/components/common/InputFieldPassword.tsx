import React from "react";
import { InputFieldProps } from "../../types/inputType";

export const InputFieldPassword: React.FC<InputFieldProps> = ({
  label,
  name,
  value,
  editing,
  onChange,
}) => (
  <div>
    <p className="label">{label}:</p>
    {editing ? (
      <input
        className="input"
        type="password"
        name={name}
        value={value}
        onChange={onChange}
        disabled={!editing}
      />
    ) : (
      <p>{value}</p>
    )}
  </div>
);
