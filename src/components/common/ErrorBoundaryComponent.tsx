import { FallbackProps } from "react-error-boundary";

export function ErrorBoundaryComponent(props: FallbackProps) {
  const { error, resetErrorBoundary } = props;

  return (
    <div className="h-screen flex flex-col justify-center items-center gap-5 bg-lightViolet">
      <h2>Something went wrong</h2>
      <p>{error.message}</p>
      <button className="button" onClick={resetErrorBoundary}>
        Reload Page
      </button>
    </div>
  );
}
