import { ErrorMessageProps } from "../../types/commonType";

export const Message = ({ message }: ErrorMessageProps) => {
  return <p className="text-standardViolet font-bold text-center">{message}</p>;
};
