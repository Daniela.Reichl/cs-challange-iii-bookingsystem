import React from "react";
import { InputFieldSelectDepartmentProps } from "../../types/inputType";

export const InputFieldSelectDepartments: React.FC<
  InputFieldSelectDepartmentProps
> = ({ label, name, editing, value, options, onChange }) => (
  <div>
    <p className="label">{label}:</p>
    {editing ? (
      <select
        className="input"
        name={name}
        value={value}
        onChange={onChange}
        disabled={!editing}
      >
        {options.map((departmentName) => (
          <option key={departmentName} value={departmentName}>
            {departmentName}
          </option>
        ))}
      </select>
    ) : (
      <p>{value}</p>
    )}
  </div>
);
