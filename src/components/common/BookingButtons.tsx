import { useNavigate } from "react-router-dom";
import { BookingButtonsProps } from "../../types/bookingType";

export const BookingButtons = ({ deskId, deskLabel }: BookingButtonsProps) => {
  //send data to booking page & fixdesk request
  const navigate = useNavigate();
  const handleNavigation = (pathname: string) => {
    const linkProps = {
      deskId: deskId,
      deskLabel: deskLabel,
    };
    navigate({
      pathname,
      search: `?deskId=${linkProps.deskId}&deskLabel=${linkProps.deskLabel}`,
    });
  };

  //Forward to booking page
  const handleBooking = () => {
    handleNavigation("/book/summary");
  };
  //Forward to fixdesk request
  const handleFixdesk = () => {
    handleNavigation("/request/summary");
  };
  return (
    <div className="flex justify-center items-center gap-4 w-full mt-5">
      {/* Button Fix desk request */}
      <button className="buttonFit" onClick={handleFixdesk}>
        Fix Desk anfragen
      </button>
      {/* Button flex desk request */}
      <button className="buttonFit" onClick={handleBooking}>
        Flex Desk buchen
      </button>
    </div>
  );
};
