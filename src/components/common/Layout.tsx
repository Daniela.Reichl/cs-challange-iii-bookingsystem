import { Outlet } from "react-router-dom";
import { Header } from "./Header";
import { Footer } from "./Footer";
import { AutoRefreshToken } from "../main/login/AutoRefreshToken";

export const Layout = () => {
  return (
    <section className="flex flex-col h-screen justify-between">
      <Header />
      <main>
        <Outlet />
      </main>
      <AutoRefreshToken />
      <Footer />
    </section>
  );
};
