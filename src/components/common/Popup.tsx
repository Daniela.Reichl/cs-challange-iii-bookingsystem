import { PopupProps } from "../../types/commonType";

export const Popup = ({ message, handleSaveClick, onClose }: PopupProps) => {
  return (
    <div className="h-full w-full fixed top-0 left-0 flex justify-center text-center items-center z-10 bg-black bg-opacity-30">
      {/* Popup */}
      <div className="baseContent bg-white m-5 sm:m-10">
        {/* Popup message */}
        <h2 className=" text-lg sm:text-2xl md:text-4xl">{message}</h2>
        <div className="flex gap-5">
          {/* Close button */}
          <button className="button" onClick={onClose}>
            Abbrechen
          </button>
          {/* Confirm button */}
          <button className="button" onClick={handleSaveClick}>
            Ja
          </button>
        </div>
      </div>
    </div>
  );
};
