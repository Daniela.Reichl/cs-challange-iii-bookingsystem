import { useEffect, useRef, useState } from "react";
import { Navbar } from "./Navbar";
import avatarWhite from "/src/assets/icons/avatar-white.svg";
import logo from "/src/assets/logo/LogoIcon.svg";
import { getLoginToken } from "../../config/config";
import { Link } from "react-router-dom";

export const Header = () => {
  const [showNavbar, setShowNavbar] = useState<boolean>(false);
  const [loginToken, setLoginToken] = useState<string | null>(null);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const headerRef = useRef<HTMLDivElement | null>(null);

  // Check if user have a token
  useEffect(() => {
    const token = getLoginToken();
    setLoginToken(token);
    setIsLoading(false);
  }, []);

  // Reload page after login to display avatarIcon
  window.addEventListener("loginSuccessful", () => {
    window.location.reload();
  });

  // Show navbar when a link is clicked
  const handleToggleNavbar = () => {
    setShowNavbar(!showNavbar);
  };

  // Close the navbar when a link is clicked
  const handleLinkClick = () => {
    setShowNavbar(false); //
  };

  // close navbar if user clicks outside the navbar
  useEffect(() => {
    if (showNavbar) {
      const handleOutsideClick = (event: MouseEvent) => {
        if (!headerRef.current?.contains(event.target as Node)) {
          setShowNavbar(false);
        }
      };
      document.addEventListener("click", handleOutsideClick);

      return () => {
        document.removeEventListener("click", handleOutsideClick);
      };
    }
  }, [showNavbar]);

  return (
    <div className="bg-lightViolet flex justify-between items-center px-5 md:px-10 py-5">
      <Link to="/" className="flex gap-2">
        <img
          className="h-14 hover:scale-110"
          src={logo}
          alt="Logo Booking Solutions"
        />
        <div>
          <h3 className="text-standardViolet">Doneus & Reichl</h3>
          <p className="text-darkViolet">Booking Solutions</p>
        </div>
      </Link>
      {loginToken && (
        <div ref={headerRef}>
          <img
            src={avatarWhite}
            alt="avatar icon"
            className={`h-12 md:h-16 p-2 md:p-3 rounded-full bg-standardViolet relative cursor-pointer hover:scale-105`}
            onClick={handleToggleNavbar}
          />
          <Navbar showLinks={showNavbar} onClickLink={handleLinkClick} />
        </div>
      )}
    </div>
  );
};
