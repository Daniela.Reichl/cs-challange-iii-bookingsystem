import { Link } from "react-router-dom";

export const GDPRPage = () => {
  return (
    <div className="baseFlexStyle w-4/5 m-2 md:m-10">
      <h1 className="text-standardViolet">Datenschutzerklärung</h1>
      <div>
        <h2 id="m4158">Präambel</h2>
        <p>
          Mit der folgenden Datenschutzerklärung möchten wir Sie darüber
          aufklären, welche Arten Ihrer personenbezogenen Daten (nachfolgend
          auch kurz als "Daten“ bezeichnet) wir zu welchen Zwecken und in
          welchem Umfang im Rahmen der Bereitstellung unserer Applikation
          verarbeiten.
        </p>
        <p>Die verwendeten Begriffe sind nicht geschlechtsspezifisch.</p>
        <p>Stand: 7. August 2023</p>
      </div>
      <h2>Inhaltsübersicht</h2>
      <ul className="index underline text-darkViolet ">
        <li>
          <a className="index-link" href="#m4158">
            Präambel
          </a>
        </li>
        <li>
          <a className="index-link" href="#m3">
            Verantwortlicher
          </a>
        </li>
        <li>
          <a className="index-link" href="#mOverview">
            Übersicht der Verarbeitungen
          </a>
        </li>
        <li>
          <a className="index-link" href="#m2427">
            Maßgebliche Rechtsgrundlagen
          </a>
        </li>
        <li>
          <a className="index-link" href="#m27">
            Sicherheitsmaßnahmen
          </a>
        </li>
        <li>
          <a className="index-link" href="#m25">
            Übermittlung von personenbezogenen Daten
          </a>
        </li>
        <li>
          <a className="index-link" href="#m24">
            Internationale Datentransfers
          </a>
        </li>
        <li>
          <a className="index-link" href="#m12">
            Löschung von Daten
          </a>
        </li>
        <li>
          <a className="index-link" href="#m10">
            Rechte der betroffenen Personen
          </a>
        </li>
        <li>
          <a className="index-link" href="#m134">
            Einsatz von Cookies
          </a>
        </li>
        <li>
          <a className="index-link" href="#m317">
            Geschäftliche Leistungen
          </a>
        </li>
        <li>
          <a className="index-link" href="#m367">
            Registrierung, Anmeldung und Nutzerkonto
          </a>
        </li>
        <li>
          <a className="index-link" href="#m15">
            Änderung und Aktualisierung der Datenschutzerklärung
          </a>
        </li>
      </ul>
      <h2 id="m3">Verantwortlicher</h2>
      <div className="flex flex-col gap-1">
        <p>Max Mustermann</p>
        <p>Alpen-Adria-Platz 50</p>
        <p>9020 Klagenfurt</p>
        <p>E-Mail-Adresse:</p> <p>M.Mustermann@doneus-reichl.at</p>
      </div>
      <h2 id="m2427">Maßgebliche Rechtsgrundlagen</h2>
      <p>
        <strong>Maßgebliche Rechtsgrundlagen nach der DSGVO: </strong>Im
        Folgenden erhalten Sie eine Übersicht der Rechtsgrundlagen der DSGVO,
        auf deren Basis wir personenbezogene Daten verarbeiten. Bitte nehmen Sie
        zur Kenntnis, dass neben den Regelungen der DSGVO nationale
        Datenschutzvorgaben in Ihrem bzw. unserem Wohn- oder Sitzland gelten
        können. Sollten ferner im Einzelfall speziellere Rechtsgrundlagen
        maßgeblich sein, teilen wir Ihnen diese in der Datenschutzerklärung mit.
      </p>
      <ul>
        <li>
          <strong>Einwilligung (Art. 6 Abs. 1 S. 1 lit. a) DSGVO)</strong> - Die
          betroffene Person hat ihre Einwilligung in die Verarbeitung der sie
          betreffenden personenbezogenen Daten für einen spezifischen Zweck oder
          mehrere bestimmte Zwecke gegeben.
        </li>
        <li>
          <strong>
            Vertragserfüllung und vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1
            lit. b) DSGVO)
          </strong>
          - Die Verarbeitung ist für die Erfüllung eines Vertrags, dessen
          Vertragspartei die betroffene Person ist, oder zur Durchführung
          vorvertraglicher Maßnahmen erforderlich, die auf Anfrage der
          betroffenen Person erfolgen.
        </li>
        <li>
          <strong>
            Rechtliche Verpflichtung (Art. 6 Abs. 1 S. 1 lit. c) DSGVO)
          </strong>
          - Die Verarbeitung ist zur Erfüllung einer rechtlichen Verpflichtung
          erforderlich, der der Verantwortliche unterliegt.
        </li>
        <li>
          <strong>
            Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f) DSGVO)
          </strong>
          - Die Verarbeitung ist zur Wahrung der berechtigten Interessen des
          Verantwortlichen oder eines Dritten erforderlich, sofern nicht die
          Interessen oder Grundrechte und Grundfreiheiten der betroffenen
          Person, die den Schutz personenbezogener Daten erfordern, überwiegen.
        </li>
      </ul>
      <p>
        <strong>Nationale Datenschutzregelungen in Österreich: </strong>
        Zusätzlich zu den Datenschutzregelungen der DSGVO gelten nationale
        Regelungen zum Datenschutz in Österreich. Hierzu gehört insbesondere das
        Bundesgesetz zum Schutz natürlicher Personen bei der Verarbeitung
        personenbezogener Daten (Datenschutzgesetz – DSG). Das Datenschutzgesetz
        enthält insbesondere Spezialregelungen zum Recht auf Auskunft, zum Recht
        auf Richtigstellung oder Löschung, zur Verarbeitung besonderer
        Kategorien personenbezogener Daten, zur Verarbeitung für andere Zwecke
        und zur Übermittlung sowie zur automatisierten Entscheidungsfindung im
        Einzelfall.
      </p>
      <p>
        <strong>Hinweis auf Geltung DSGVO und Schweizer DSG: </strong>Diese
        Datenschutzhinweise dienen sowohl der Informationserteilung nach dem
        schweizerischen Bundesgesetz über den Datenschutz (Schweizer DSG) als
        auch nach der Datenschutzgrundverordnung (DSGVO). Aus diesem Grund
        bitten wir Sie zu beachten, dass aufgrund der breiteren räumlichen
        Anwendung und Verständlichkeit die Begriffe der DSGVO verwendet werden.
        Insbesondere statt der im Schweizer DSG verwendeten Begriffe
        „Bearbeitung“ von „Personendaten“, "überwiegendes Interesse" und
        "besonders schützenswerte Personendaten" werden die in der DSGVO
        verwendeten Begriffe „Verarbeitung“ von „personenbezogenen Daten“ sowie
        "berechtigtes Interesse" und "besondere Kategorien von Daten" verwendet.
        Die gesetzliche Bedeutung der Begriffe wird jedoch im Rahmen der Geltung
        des Schweizer DSG weiterhin nach dem Schweizer DSG bestimmt.
      </p>
      <h2 id="mOverview">Übersicht der Verarbeitungen</h2>
      <p>
        Die nachfolgende Übersicht fasst die Arten der verarbeiteten Daten und
        die Zwecke ihrer Verarbeitung zusammen und verweist auf die betroffenen
        Personen.
      </p>
      <h3>Arten der verarbeiteten Daten</h3>
      <ul>
        <li>Bestandsdaten.</li>
        <li>Zahlungsdaten.</li>
        <li>Kontaktdaten.</li>
        <li>Inhaltsdaten.</li>
        <li>Vertragsdaten.</li>
        <li>Nutzungsdaten.</li>
        <li>Meta-, Kommunikations- und Verfahrensdaten.</li>
      </ul>
      <h3>Kategorien betroffener Personen</h3>
      <ul>
        <li>Kunden.</li>
        <li>Interessenten.</li>
        <li>Nutzer.</li>
        <li>Geschäfts- und Vertragspartner.</li>
      </ul>
      <h3>Zwecke der Verarbeitung</h3>
      <ul>
        <li>
          Erbringung vertraglicher Leistungen und erfüllung vertraglicher
          Pflichten.
        </li>
        <li>Kontaktanfragen und Kommunikation.</li>
        <li>Sicherheitsmaßnahmen.</li>
        <li>Büro- und Organisationsverfahren.</li>
        <li>Konversionsmessung.</li>
        <li>Verwaltung und Beantwortung von Anfragen.</li>
        <li>Profile mit nutzerbezogenen Informationen.</li>
        <li>
          Bereitstellung unseres Onlineangebotes und Nutzerfreundlichkeit.
        </li>
      </ul>
      <h2 id="m27">Sicherheitsmaßnahmen</h2>
      <p>
        Wir treffen nach Maßgabe der gesetzlichen Vorgaben unter
        Berücksichtigung des Stands der Technik, der Implementierungskosten und
        der Art, des Umfangs, der Umstände und der Zwecke der Verarbeitung sowie
        der unterschiedlichen Eintrittswahrscheinlichkeiten und des Ausmaßes der
        Bedrohung der Rechte und Freiheiten natürlicher Personen geeignete
        technische und organisatorische Maßnahmen, um ein dem Risiko
        angemessenes Schutzniveau zu gewährleisten.
      </p>
      <p>
        Zu den Maßnahmen gehören insbesondere die Sicherung der Vertraulichkeit,
        Integrität und Verfügbarkeit von Daten durch Kontrolle des physischen
        und elektronischen Zugangs zu den Daten als auch des sie betreffenden
        Zugriffs, der Eingabe, der Weitergabe, der Sicherung der Verfügbarkeit
        und ihrer Trennung. Des Weiteren haben wir Verfahren eingerichtet, die
        eine Wahrnehmung von Betroffenenrechten, die Löschung von Daten und
        Reaktionen auf die Gefährdung der Daten gewährleisten. Ferner
        berücksichtigen wir den Schutz personenbezogener Daten bereits bei der
        Entwicklung bzw. Auswahl von Hardware, Software sowie Verfahren
        entsprechend dem Prinzip des Datenschutzes, durch Technikgestaltung und
        durch datenschutzfreundliche Voreinstellungen.
      </p>
      <p>
        Kürzung der IP-Adresse: Sofern IP-Adressen von uns oder von den
        eingesetzten Dienstleistern und Technologien verarbeitet werden und die
        Verarbeitung einer vollständigen IP-Adresse nicht erforderlich ist, wird
        die IP-Adresse gekürzt (auch als "IP-Masking" bezeichnet). Hierbei
        werden die letzten beiden Ziffern, bzw. der letzte Teil der IP-Adresse
        nach einem Punkt entfernt, bzw. durch Platzhalter ersetzt. Mit der
        Kürzung der IP-Adresse soll die Identifizierung einer Person anhand
        ihrer IP-Adresse verhindert oder wesentlich erschwert werden.
      </p>
      <p>
        TLS-Verschlüsselung (https): Um Ihre via unserem Online-Angebot
        übermittelten Daten zu schützen, nutzen wir eine TLS-Verschlüsselung.
        Sie erkennen derart verschlüsselte Verbindungen an dem Präfix https://
        in der Adresszeile Ihres Browsers.
      </p>
      <h2 id="m25">Übermittlung von personenbezogenen Daten</h2>
      <p>
        Im Rahmen unserer Verarbeitung von personenbezogenen Daten kommt es vor,
        dass die Daten an andere Stellen, Unternehmen, rechtlich selbstständige
        Organisationseinheiten oder Personen übermittelt oder sie ihnen
        gegenüber offengelegt werden. Zu den Empfängern dieser Daten können z.B.
        mit IT-Aufgaben beauftragte Dienstleister oder Anbieter von Diensten und
        Inhalten, die in eine Webseite eingebunden werden, gehören. In solchen
        Fällen beachten wir die gesetzlichen Vorgaben und schließen insbesondere
        entsprechende Verträge bzw. Vereinbarungen, die dem Schutz Ihrer Daten
        dienen, mit den Empfängern Ihrer Daten ab.
      </p>
      <p>
        Datenübermittlung innerhalb der Organisation: Wir können
        personenbezogene Daten an andere Stellen innerhalb unserer Organisation
        übermitteln oder ihnen den Zugriff auf diese Daten gewähren. Sofern
        diese Weitergabe zu administrativen Zwecken erfolgt, beruht die
        Weitergabe der Daten auf unseren berechtigten unternehmerischen und
        betriebswirtschaftlichen Interessen oder erfolgt, sofern sie Erfüllung
        unserer vertragsbezogenen Verpflichtungen erforderlich ist oder wenn
        eine Einwilligung der Betroffenen oder eine gesetzliche Erlaubnis
        vorliegt.
      </p>
      <h2 id="m24">Internationale Datentransfers</h2>
      <p>
        Datenverarbeitung in Drittländern: Sofern wir Daten in einem Drittland
        (d.h., außerhalb der Europäischen Union (EU), des Europäischen
        Wirtschaftsraums (EWR)) verarbeiten oder die Verarbeitung im Rahmen der
        Inanspruchnahme von Diensten Dritter oder der Offenlegung bzw.
        Übermittlung von Daten an andere Personen, Stellen oder Unternehmen
        stattfindet, erfolgt dies nur im Einklang mit den gesetzlichen Vorgaben.
      </p>
      <p>
        Vorbehaltlich ausdrücklicher Einwilligung oder vertraglich oder
        gesetzlich erforderlicher Übermittlung (s. Art. 49 DSGVO) verarbeiten
        oder lassen wir die Daten nur in Drittländern mit einem anerkannten
        Datenschutzniveau (Art. 45 DSGVO), beim Vorliegen und Einhaltung
        vertraglichen Verpflichtung durch sogenannte Standardschutzklauseln der
        EU-Kommission (Art. 46 DSGVO) oder beim Vorliegen von Zertifizierungen
        oder verbindlicher internen Datenschutzvorschriften (s. Art. 44 bis 49
        DSGVO, Informationsseite der EU-Kommission:
        <a
          href="https://ec.europa.eu/info/law/law-topic/data-protection/international-dimension-data-protection_de"
          target="_blank"
        >
          https://ec.europa.eu/info/law/law-topic/data-protection/international-dimension-data-protection_de
        </a>
        ).
      </p>
      <p>
        EU-US Trans-Atlantic Data Privacy Framework: Im Rahmen des sogenannten
        „Data Privacy Framework” (DPF) hat die EU-Kommission das
        Datenschutzniveau ebenfalls für bestimmte Unternehmen aus den USA im
        Rahmen der Angemessenheitsbeschlusses vom 10.07.2023 als sicher
        anerkannt. Die Liste der zertifizierten Unternehmen als auch weitere
        Informationen zu dem DPF können Sie der Webseite des Handelsministeriums
        der USA unter
        <a href="https://www.dataprivacyframework.gov/" target="_blank">
          https://www.dataprivacyframework.gov/
        </a>
        (in Englisch) entnehmen. Wir informieren Sie im Rahmen der
        Datenschutzhinweise welche von uns eingesetzten Diensteanbieter unter
        dem Data Privacy Framework zertifiziert sind.
      </p>
      <h2 id="m12">Löschung von Daten</h2>
      <p>
        Die von uns verarbeiteten Daten werden nach Maßgabe der gesetzlichen
        Vorgaben gelöscht, sobald deren zur Verarbeitung erlaubten
        Einwilligungen widerrufen werden oder sonstige Erlaubnisse entfallen
        (z.B. wenn der Zweck der Verarbeitung dieser Daten entfallen ist oder
        sie für den Zweck nicht erforderlich sind). Sofern die Daten nicht
        gelöscht werden, weil sie für andere und gesetzlich zulässige Zwecke
        erforderlich sind, wird deren Verarbeitung auf diese Zwecke beschränkt.
        D.h., die Daten werden gesperrt und nicht für andere Zwecke verarbeitet.
        Das gilt z.B. für Daten, die aus handels- oder steuerrechtlichen Gründen
        aufbewahrt werden müssen oder deren Speicherung zur Geltendmachung,
        Ausübung oder Verteidigung von Rechtsansprüchen oder zum Schutz der
        Rechte einer anderen natürlichen oder juristischen Person erforderlich
        ist.
      </p>
      <p>
        Unsere Datenschutzhinweise können ferner weitere Angaben zu der
        Aufbewahrung und Löschung von Daten beinhalten, die für die jeweiligen
        Verarbeitungen vorrangig gelten.
      </p>
      <h2 id="m10">Rechte der betroffenen Personen</h2>
      <p>
        Rechte der betroffenen Personen aus der DSGVO: Ihnen stehen als
        Betroffene nach der DSGVO verschiedene Rechte zu, die sich insbesondere
        aus Art. 15 bis 21 DSGVO ergeben:
      </p>
      <ul>
        <li>
          <strong>
            Widerspruchsrecht: Sie haben das Recht, aus Gründen, die sich aus
            Ihrer besonderen Situation ergeben, jederzeit gegen die Verarbeitung
            der Sie betreffenden personenbezogenen Daten, die aufgrund von Art.
            6 Abs. 1 lit. e oder f DSGVO erfolgt, Widerspruch einzulegen; dies
            gilt auch für ein auf diese Bestimmungen gestütztes Profiling.
            Werden die Sie betreffenden personenbezogenen Daten verarbeitet, um
            Direktwerbung zu betreiben, haben Sie das Recht, jederzeit
            Widerspruch gegen die Verarbeitung der Sie betreffenden
            personenbezogenen Daten zum Zwecke derartiger Werbung einzulegen;
            dies gilt auch für das Profiling, soweit es mit solcher
            Direktwerbung in Verbindung steht.
          </strong>
        </li>
        <li>
          <strong>Widerrufsrecht bei Einwilligungen:</strong> Sie haben das
          Recht, erteilte Einwilligungen jederzeit zu widerrufen.
        </li>
        <li>
          <strong>Auskunftsrecht:</strong> Sie haben das Recht, eine Bestätigung
          darüber zu verlangen, ob betreffende Daten verarbeitet werden und auf
          Auskunft über diese Daten sowie auf weitere Informationen und Kopie
          der Daten entsprechend den gesetzlichen Vorgaben.
        </li>
        <li>
          <strong>Recht auf Berichtigung:</strong> Sie haben entsprechend den
          gesetzlichen Vorgaben das Recht, die Vervollständigung der Sie
          betreffenden Daten oder die Berichtigung der Sie betreffenden
          unrichtigen Daten zu verlangen.
        </li>
        <li>
          <strong>
            Recht auf Löschung und Einschränkung der Verarbeitung:
          </strong>
          Sie haben nach Maßgabe der gesetzlichen Vorgaben das Recht, zu
          verlangen, dass Sie betreffende Daten unverzüglich gelöscht werden,
          bzw. alternativ nach Maßgabe der gesetzlichen Vorgaben eine
          Einschränkung der Verarbeitung der Daten zu verlangen.
        </li>
        <li>
          <strong>Recht auf Datenübertragbarkeit:</strong> Sie haben das Recht,
          Sie betreffende Daten, die Sie uns bereitgestellt haben, nach Maßgabe
          der gesetzlichen Vorgaben in einem strukturierten, gängigen und
          maschinenlesbaren Format zu erhalten oder deren Übermittlung an einen
          anderen Verantwortlichen zu fordern.
        </li>
        <li>
          <strong>Beschwerde bei Aufsichtsbehörde:</strong> Sie haben
          unbeschadet eines anderweitigen verwaltungsrechtlichen oder
          gerichtlichen Rechtsbehelfs das Recht auf Beschwerde bei einer
          Aufsichtsbehörde, insbesondere in dem Mitgliedstaat ihres gewöhnlichen
          Aufenthaltsorts, ihres Arbeitsplatzes oder des Orts des mutmaßlichen
          Verstoßes, wenn Sie der Ansicht sind, dass die Verarbeitung der Sie
          betreffenden personenbezogenen Daten gegen die Vorgaben der DSGVO
          verstößt.
        </li>
      </ul>
      <h2 id="m134">Einsatz von Cookies</h2>
      <p>
        Cookies sind kleine Textdateien, bzw. sonstige Speichervermerke, die
        Informationen auf Endgeräten speichern und Informationen aus den
        Endgeräten auslesen. Z.B. um den Login-Status in einem Nutzerkonto,
        einen Warenkorbinhalt in einem E-Shop, die aufgerufenen Inhalte oder
        verwendete Funktionen eines Onlineangebotes speichern. Cookies können
        ferner zu unterschiedlichen Zwecken eingesetzt werden, z.B. zu Zwecken
        der Funktionsfähigkeit, Sicherheit und Komfort von Onlineangeboten sowie
        der Erstellung von Analysen der Besucherströme.
      </p>
      <p>
        <strong>Hinweise zur Einwilligung: </strong>Wir setzen Cookies im
        Einklang mit den gesetzlichen Vorschriften ein. Daher holen wir von den
        Nutzern eine vorhergehende Einwilligung ein, außer wenn diese gesetzlich
        nicht gefordert ist. Eine Einwilligung ist insbesondere nicht notwendig,
        wenn das Speichern und das Auslesen der Informationen, also auch von
        Cookies, unbedingt erforderlich sind, um dem den Nutzern einen von ihnen
        ausdrücklich gewünschten Telemediendienst (also unser Onlineangebot) zur
        Verfügung zu stellen. Zu den unbedingt erforderlichen Cookies gehören in
        der Regel Cookies mit Funktionen, die der Anzeige und Lauffähigkeit des
        Onlineangebotes , dem Lastausgleich, der Sicherheit, der Speicherung der
        Präferenzen und Auswahlmöglichkeiten der Nutzer oder ähnlichen mit der
        Bereitstellung der Haupt- und Nebenfunktionen des von den Nutzern
        angeforderten Onlineangebotes zusammenhängenden Zwecken dienen. Die
        widerrufliche Einwilligung wird gegenüber den Nutzern deutlich
        kommuniziert und enthält die Informationen zu der jeweiligen
        Cookie-Nutzung.
      </p>
      <p>
        <strong>Hinweise zu datenschutzrechtlichen Rechtsgrundlagen: </strong>
        Auf welcher datenschutzrechtlichen Rechtsgrundlage wir die
        personenbezogenen Daten der Nutzer mit Hilfe von Cookies verarbeiten,
        hängt davon ab, ob wir Nutzer um eine Einwilligung bitten. Falls die
        Nutzer einwilligen, ist die Rechtsgrundlage der Verarbeitung Ihrer Daten
        die erklärte Einwilligung. Andernfalls werden die mithilfe von Cookies
        verarbeiteten Daten auf Grundlage unserer berechtigten Interessen (z.B.
        an einem betriebswirtschaftlichen Betrieb unseres Onlineangebotes und
        Verbesserung seiner Nutzbarkeit) verarbeitet oder, wenn dies im Rahmen
        der Erfüllung unserer vertraglichen Pflichten erfolgt, wenn der Einsatz
        von Cookies erforderlich ist, um unsere vertraglichen Verpflichtungen zu
        erfüllen. Zu welchen Zwecken die Cookies von uns verarbeitet werden,
        darüber klären wir im Laufe dieser Datenschutzerklärung oder im Rahmen
        von unseren Einwilligungs- und Verarbeitungsprozessen auf.
      </p>
      <p>
        <strong>Speicherdauer: </strong>Im Hinblick auf die Speicherdauer werden
        die folgenden Arten von Cookies unterschieden:
      </p>
      <ul>
        <li>
          <strong>
            Temporäre Cookies (auch: Session- oder Sitzungs-Cookies):
          </strong>
           Temporäre Cookies werden spätestens gelöscht, nachdem ein Nutzer ein
          Online-Angebot verlassen und sein Endgerät (z.B. Browser oder mobile
          Applikation) geschlossen hat.
        </li>
        <li>
          <strong>Permanente Cookies:</strong> Permanente Cookies bleiben auch
          nach dem Schließen des Endgerätes gespeichert. So können
          beispielsweise der Login-Status gespeichert oder bevorzugte Inhalte
          direkt angezeigt werden, wenn der Nutzer eine Website erneut besucht.
          Ebenso können die mit Hilfe von Cookies erhobenen Daten der Nutzer zur
          Reichweitenmessung verwendet werden. Sofern wir Nutzern keine
          expliziten Angaben zur Art und Speicherdauer von Cookies mitteilen (z.
          B. im Rahmen der Einholung der Einwilligung), sollten Nutzer davon
          ausgehen, dass Cookies permanent sind und die Speicherdauer bis zu
          zwei Jahre betragen kann.
        </li>
      </ul>
      <p>
        <strong>
          Allgemeine Hinweise zum Widerruf und Widerspruch (sog. "Opt-Out"):
        </strong>
        Nutzer können die von ihnen abgegebenen Einwilligungen jederzeit
        widerrufen und der Verarbeitung entsprechend den gesetzlichen Vorgaben
        widersprechen. Hierzu können Nutzer unter anderem die Verwendung von
        Cookies in den Einstellungen ihres Browsers einschränken (wobei dadurch
        auch die Funktionalität unseres Onlineangebotes eingeschränkt sein
        kann). Ein Widerspruch gegen die Verwendung von Cookies zu
        Online-Marketing-Zwecken kann auch über die Websites
        <a href="https://optout.aboutads.info/" target="_new">
          https://optout.aboutads.info
        </a>
        und
        <a href="https://www.youronlinechoices.com/" target="_new">
          https://www.youronlinechoices.com/
        </a>
        erklärt werden.
      </p>
      <ul className="m-elements">
        <li>
          <strong>Rechtsgrundlagen:</strong> Berechtigte Interessen (Art. 6 Abs.
          1 S. 1 lit. f) DSGVO). Einwilligung (Art. 6 Abs. 1 S. 1 lit. a)
          DSGVO).
        </li>
      </ul>
      <p>
        <strong>
          Weitere Hinweise zu Verarbeitungsprozessen, Verfahren und Diensten:
        </strong>
      </p>
      <ul className="m-elements">
        <li>
          <strong>
            Verarbeitung von Cookie-Daten auf Grundlage einer Einwilligung:{" "}
          </strong>
          Wir setzen ein Verfahren zum Cookie-Einwilligungs-Management ein, in
          dessen Rahmen die Einwilligungen der Nutzer in den Einsatz von
          Cookies, bzw. der im Rahmen des
          Cookie-Einwilligungs-Management-Verfahrens genannten Verarbeitungen
          und Anbieter eingeholt sowie von den Nutzern verwaltet und widerrufen
          werden können. Hierbei wird die Einwilligungserklärung gespeichert, um
          deren Abfrage nicht erneut wiederholen zu müssen und die Einwilligung
          entsprechend der gesetzlichen Verpflichtung nachweisen zu können. Die
          Speicherung kann serverseitig und/oder in einem Cookie (sogenanntes
          Opt-In-Cookie, bzw. mithilfe vergleichbarer Technologien) erfolgen, um
          die Einwilligung einem Nutzer, bzw. dessen Gerät zuordnen zu können.
          Vorbehaltlich individueller Angaben zu den Anbietern von
          Cookie-Management-Diensten, gelten die folgenden Hinweise: Die Dauer
          der Speicherung der Einwilligung kann bis zu zwei Jahren betragen.
          Hierbei wird ein pseudonymer Nutzer-Identifikator gebildet und mit dem
          Zeitpunkt der Einwilligung, Angaben zur Reichweite der Einwilligung
          (z. B. welche Kategorien von Cookies und/oder Diensteanbieter) sowie
          dem Browser, System und verwendeten Endgerät gespeichert;{" "}
          <span className="">
            <strong>Rechtsgrundlagen:</strong> Einwilligung (Art. 6 Abs. 1 S. 1
            lit. a) DSGVO).
          </span>
        </li>
      </ul>
      <h2 id="m317">Geschäftliche Leistungen</h2>
      <p>
        Wir verarbeiten Daten unserer Vertrags- und Geschäftspartner, z.B.
        Kunden und Interessenten (zusammenfassend bezeichnet als
        "Vertragspartner") im Rahmen von vertraglichen und vergleichbaren
        Rechtsverhältnissen sowie damit verbundenen Maßnahmen und im Rahmen der
        Kommunikation mit den Vertragspartnern (oder vorvertraglich), z.B., um
        Anfragen zu beantworten.
      </p>
      <p>
        Wir verarbeiten diese Daten, um unsere vertraglichen Verpflichtungen zu
        erfüllen. Dazu gehören insbesondere die Verpflichtungen zur Erbringung
        der vereinbarten Leistungen, etwaige Aktualisierungspflichten und
        Abhilfe bei Gewährleistungs- und sonstigen Leistungsstörungen. Darüber
        hinaus verarbeiten wir die Daten zur Wahrung unserer Rechte und zum
        Zwecke der mit diesen Pflichten verbundenen Verwaltungsaufgaben sowie
        der Unternehmensorganisation. Darüber hinaus verarbeiten wir die Daten
        auf Grundlage unserer berechtigten Interessen an einer ordnungsgemäßen
        und betriebswirtschaftlichen Geschäftsführung sowie an
        Sicherheitsmaßnahmen zum Schutz unserer Vertragspartner und unseres
        Geschäftsbetriebes vor Missbrauch, Gefährdung ihrer Daten, Geheimnisse,
        Informationen und Rechte (z.B. zur Beteiligung von Telekommunikations-,
        Transport- und sonstigen Hilfsdiensten sowie Subunternehmern, Banken,
        Steuer- und Rechtsberatern, Zahlungsdienstleistern oder Finanzbehörden).
        Im Rahmen des geltenden Rechts geben wir die Daten von Vertragspartnern
        nur insoweit an Dritte weiter, als dies für die vorgenannten Zwecke oder
        zur Erfüllung gesetzlicher Pflichten erforderlich ist. Über weitere
        Formen der Verarbeitung, z.B. zu Marketingzwecken, werden die
        Vertragspartner im Rahmen dieser Datenschutzerklärung informiert.
      </p>
      <p>
        Welche Daten für die vorgenannten Zwecke erforderlich sind, teilen wir
        den Vertragspartnern vor oder im Rahmen der Datenerhebung, z.B. in
        Onlineformularen, durch besondere Kennzeichnung (z.B. Farben) bzw.
        Symbole (z.B. Sternchen o.ä.), oder persönlich mit.
      </p>
      <p>
        Wir löschen die Daten nach Ablauf gesetzlicher Gewährleistungs- und
        vergleichbarer Pflichten, d.h., grundsätzlich nach Ablauf von 4 Jahren,
        es sei denn, dass die Daten in einem Kundenkonto gespeichert werden,
        z.B., solange sie aus gesetzlichen Gründen der Archivierung aufbewahrt
        werden müssen. Die gesetzliche Aufbewahrungsfrist beträgt bei
        steuerrechtlich relevanten Unterlagen sowie bei Handelsbüchern,
        Inventaren, Eröffnungsbilanzen, Jahresabschlüssen, die zum Verständnis
        dieser Unterlagen erforderlichen Arbeitsanweisungen und sonstigen
        Organisationsunterlagen und Buchungsbelegen zehn Jahre sowie bei
        empfangenen Handels- und Geschäftsbriefen und Wiedergaben der
        abgesandten Handels- und Geschäftsbriefe sechs Jahre. Die Frist beginnt
        mit Ablauf des Kalenderjahres, in dem die letzte Eintragung in das Buch
        gemacht, das Inventar, die Eröffnungsbilanz, der Jahresabschluss oder
        der Lagebericht aufgestellt, der Handels- oder Geschäftsbrief empfangen
        oder abgesandt worden oder der Buchungsbeleg entstanden ist, ferner die
        Aufzeichnung vorgenommen worden ist oder die sonstigen Unterlagen
        entstanden sind.
      </p>
      <p>
        Soweit wir zur Erbringung unserer Leistungen Drittanbieter oder
        Plattformen einsetzen, gelten im Verhältnis zwischen den Nutzern und den
        Anbietern die Geschäftsbedingungen und Datenschutzhinweise der
        jeweiligen Drittanbieter oder Plattformen.
      </p>
      <ul className="m-elements">
        <li>
          <strong>Verarbeitete Datenarten:</strong> Bestandsdaten (z.B. Namen,
          Adressen); Zahlungsdaten (z.B. Bankverbindungen, Rechnungen,
          Zahlungshistorie); Kontaktdaten (z.B. E-Mail, Telefonnummern);
          Vertragsdaten (z.B. Vertragsgegenstand, Laufzeit, Kundenkategorie);
          Nutzungsdaten (z.B. besuchte Webseiten, Interesse an Inhalten,
          Zugriffszeiten). Meta-, Kommunikations- und Verfahrensdaten (z. B.
          IP-Adressen, Zeitangaben, Identifikationsnummern,
          Einwilligungsstatus).
        </li>
        <li>
          <strong>Betroffene Personen:</strong> Kunden; Interessenten.
          Geschäfts- und Vertragspartner.
        </li>
        <li>
          <strong>Zwecke der Verarbeitung:</strong> Erbringung vertraglicher
          Leistungen und erfüllung vertraglicher Pflichten;
          Sicherheitsmaßnahmen; Kontaktanfragen und Kommunikation; Büro- und
          Organisationsverfahren; Verwaltung und Beantwortung von Anfragen;
          Konversionsmessung (Messung der Effektivität von Marketingmaßnahmen).
          Profile mit nutzerbezogenen Informationen (Erstellen von
          Nutzerprofilen).
        </li>
        <li className="">
          <strong>Rechtsgrundlagen:</strong> Vertragserfüllung und
          vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1 lit. b) DSGVO);
          Rechtliche Verpflichtung (Art. 6 Abs. 1 S. 1 lit. c) DSGVO).
          Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f) DSGVO).
        </li>
      </ul>
      <p>
        <strong>
          Weitere Hinweise zu Verarbeitungsprozessen, Verfahren und Diensten:
        </strong>
      </p>
      <ul className="m-elements">
        <li>
          <strong>Kundenkonto: </strong>Kunden können innerhalb unseres
          Onlineangebotes ein Konto anlegen (z.B. Kunden- bzw. Nutzerkonto, kurz
          "Kundenkonto"). Falls die Registrierung eines Kundenkontos
          erforderlich ist, werden Kunden hierauf ebenso hingewiesen wie auf die
          für die Registrierung erforderlichen Angaben. Die Kundenkonten sind
          nicht öffentlich und können von Suchmaschinen nicht indexiert werden.
          Im Rahmen der Registrierung sowie anschließender Anmeldungen und
          Nutzungen des Kundenkontos speichern wir die IP-Adressen der Kunden
          nebst den Zugriffszeitpunkten, um die Registrierung nachweisen und
          etwaigem Missbrauch des Kundenkontos vorbeugen zu können. Wurde das
          Kundenkonto gekündigt, werden die Daten des Kundenkontos nach dem
          Kündigungszeitpunkt gelöscht, sofern sie nicht für andere Zwecke als
          die Bereitstellung im Kundenkonto aufbewahrt werden oder aus
          rechtlichen Gründen aufbewahrt werden müssen (z.B. interne Speicherung
          von Kundendaten, Bestellvorgängen oder Rechnungen). Es liegt in der
          Verantwortung der Kunden, ihre Daten bei Kündigung des Kundenkontos zu
          sichern;
          <span>
            <strong>Rechtsgrundlagen:</strong> Vertragserfüllung und
            vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1 lit. b) DSGVO).
          </span>
        </li>
        <li>
          <strong>Wirtschaftliche Analysen und Marktforschung: </strong>Aus
          betriebswirtschaftlichen Gründen und um Markttendenzen, Wünsche der
          Vertragspartner und Nutzer erkennen zu können, analysieren wir die uns
          vorliegenden Daten zu Geschäftsvorgängen, Verträgen, Anfragen, etc.,
          wobei in die Gruppe der betroffenen Personen Vertragspartner,
          Interessenten, Kunden, Besucher und Nutzer unseres Onlineangebotes
          fallen können. Die Analysen erfolgen zum Zweck
          betriebswirtschaftlicher Auswertungen, des Marketings und der
          Marktforschung (z.B. zur Bestimmung von Kundengruppen mit
          unterschiedlichen Eigenschaften). Dabei können wir, sofern vorhanden,
          die Profile von registrierten Nutzern samt ihrer Angaben, z.B. zu in
          Anspruch genommenen Leistungen, berücksichtigen. Die Analysen dienen
          alleine uns und werden nicht extern offenbart, sofern es sich nicht um
          anonyme Analysen mit zusammengefassten, also anonymisierten Werten
          handelt. Ferner nehmen wir Rücksicht auf die Privatsphäre der Nutzer
          und verarbeiten die Daten zu den Analysezwecken möglichst pseudonym
          und, sofern machbar, anonym (z.B. als zusammengefasste Daten);{" "}
          <span>
            <strong>Rechtsgrundlagen:</strong> Berechtigte Interessen (Art. 6
            Abs. 1 S. 1 lit. f) DSGVO).
          </span>
        </li>
        <li>
          <strong>Projekt- und Entwicklungsleistungen: </strong>Wir verarbeiten
          die Daten unserer Kunden sowie Auftraggeber (nachfolgend einheitlich
          als "Kunden" bezeichnet), um ihnen die Auswahl, den Erwerb bzw. die
          Beauftragung der gewählten Leistungen oder Werke sowie verbundener
          Tätigkeiten als auch deren Bezahlung und Zurverfügungstellung bzw.
          Ausführung oder Erbringung zu ermöglichen. Die erforderlichen Angaben
          sind als solche im Rahmen des Auftrags-, Bestell- bzw. vergleichbaren
          Vertragsschlusses gekennzeichnet und umfassen die zur
          Leistungserbringung und Abrechnung benötigten Angaben sowie
          Kontaktinformationen, um etwaige Rücksprachen halten zu können. Soweit
          wir Zugang zu Informationen der Endkunden, Mitarbeitern oder anderer
          Personen erhalten, verarbeiten wir diese im Einklang mit den
          gesetzlichen und vertraglichen Vorgaben;{" "}
          <span>
            <strong>Rechtsgrundlagen:</strong> Vertragserfüllung und
            vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1 lit. b) DSGVO).
          </span>
        </li>
        <li>
          <strong>Angebot von Software- und Plattformleistungen: </strong>Wir
          verarbeiten die Daten unserer Nutzer, angemeldeter und etwaiger
          Testnutzer (nachfolgend einheitlich als "Nutzer" bezeichnet), um ihnen
          gegenüber unsere vertraglichen Leistungen erbringen zu können sowie
          auf Grundlage berechtigter Interessen, um die Sicherheit unseres
          Angebotes gewährleisten und es weiterentwickeln zu können. Die
          erforderlichen Angaben sind als solche im Rahmen des Auftrags-,
          Bestell- bzw. vergleichbaren Vertragsschlusses gekennzeichnet und
          umfassen die zur Leistungserbringung und Abrechnung benötigten Angaben
          sowie Kontaktinformationen, um etwaige Rücksprachen halten zu können;
          <span>
            <strong>Rechtsgrundlagen:</strong> Vertragserfüllung und
            vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1 lit. b) DSGVO).
          </span>
        </li>
        <li>
          <strong>Technische Dienstleistungen: </strong>Wir verarbeiten die
          Daten unserer Kunden sowie Auftraggeber (nachfolgend einheitlich als
          "Kunden" bezeichnet), um ihnen die Auswahl, den Erwerb bzw. die
          Beauftragung der gewählten Leistungen oder Werke sowie verbundener
          Tätigkeiten als auch deren Bezahlung und Zurverfügungstellung bzw.
          Ausführung oder Erbringung zu ermöglichen. Die erforderlichen Angaben
          sind als solche im Rahmen des Auftrags-, Bestell- bzw. vergleichbaren
          Vertragsschlusses gekennzeichnet und umfassen die zur
          Leistungserbringung und Abrechnung benötigten Angaben sowie
          Kontaktinformationen, um etwaige Rücksprachen halten zu können. Soweit
          wir Zugang zu Informationen der Endkunden, Mitarbeitern oder anderer
          Personen erhalten, verarbeiten wir diese im Einklang mit den
          gesetzlichen und vertraglichen Vorgaben;
          <span>
            <strong>Rechtsgrundlagen:</strong> Vertragserfüllung und
            vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1 lit. b) DSGVO).
          </span>
        </li>
      </ul>
      <h2 id="m367">Registrierung, Anmeldung und Nutzerkonto</h2>
      <p>
        Nutzer können ein Nutzerkonto anlegen. Im Rahmen der Registrierung
        werden den Nutzern die erforderlichen Pflichtangaben mitgeteilt und zu
        Zwecken der Bereitstellung des Nutzerkontos auf Grundlage vertraglicher
        Pflichterfüllung verarbeitet. Zu den verarbeiteten Daten gehören
        insbesondere die Login-Informationen (Nutzername, Passwort sowie eine
        E-Mail-Adresse).
      </p>
      <p>
        Im Rahmen der Inanspruchnahme unserer Registrierungs- und
        Anmeldefunktionen sowie der Nutzung des Nutzerkontos speichern wir die
        IP-Adresse und den Zeitpunkt der jeweiligen Nutzerhandlung. Die
        Speicherung erfolgt auf Grundlage unserer berechtigten Interessen als
        auch jener der Nutzer an einem Schutz vor Missbrauch und sonstiger
        unbefugter Nutzung. Eine Weitergabe dieser Daten an Dritte erfolgt
        grundsätzlich nicht, es sei denn, sie ist zur Verfolgung unserer
        Ansprüche erforderlich oder es besteht eine gesetzliche Verpflichtung
        hierzu.
      </p>
      <p>
        Die Nutzer können über Vorgänge, die für deren Nutzerkonto relevant
        sind, wie z.B. technische Änderungen, per E-Mail informiert werden.
      </p>
      <ul className="m-elements">
        <li>
          <strong>Verarbeitete Datenarten:</strong> Bestandsdaten (z.B. Namen,
          Adressen); Kontaktdaten (z.B. E-Mail, Telefonnummern); Inhaltsdaten
          (z.B. Eingaben in Onlineformularen). Meta-, Kommunikations- und
          Verfahrensdaten (z. B. IP-Adressen, Zeitangaben,
          Identifikationsnummern, Einwilligungsstatus).
        </li>
        <li>
          <strong>Betroffene Personen:</strong> Nutzer (z.B. Webseitenbesucher,
          Nutzer von Onlinediensten).
        </li>
        <li>
          <strong>Zwecke der Verarbeitung:</strong> Erbringung vertraglicher
          Leistungen und erfüllung vertraglicher Pflichten;
          Sicherheitsmaßnahmen; Verwaltung und Beantwortung von Anfragen.
          Bereitstellung unseres Onlineangebotes und Nutzerfreundlichkeit.
        </li>
        <li className="">
          <strong>Rechtsgrundlagen:</strong> Vertragserfüllung und
          vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1 lit. b) DSGVO).
          Berechtigte Interessen (Art. 6 Abs. 1 S. 1 lit. f) DSGVO).
        </li>
      </ul>
      <p>
        <strong>
          Weitere Hinweise zu Verarbeitungsprozessen, Verfahren und Diensten:
        </strong>
      </p>
      <ul className="m-elements">
        <li>
          <strong>Registrierung mit Klarnamen: </strong>Aufgrund der Natur
          unserer Community bitten wir die Nutzer unser Angebot nur unter
          Verwendung von Klarnamen zu nutzen. D.h. die Nutzung von Pseudonymen
          ist nicht zulässig;
          <span className="">
            <strong>Rechtsgrundlagen:</strong> Vertragserfüllung und
            vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1 lit. b) DSGVO).
          </span>
        </li>
        <li>
          <strong>Profile der Nutzer sind nicht öffentlich: </strong>Die Profile
          der Nutzer sind öffentlich nicht sichtbar und nicht zugänglich.
        </li>
        <li>
          <strong>Löschung von Daten nach Kündigung: </strong>Wenn Nutzer ihr
          Nutzerkonto gekündigt haben, werden deren Daten im Hinblick auf das
          Nutzerkonto, vorbehaltlich einer gesetzlichen Erlaubnis, Pflicht oder
          Einwilligung der Nutzer, gelöscht;
          <span>
            <strong>Rechtsgrundlagen:</strong> Vertragserfüllung und
            vorvertragliche Anfragen (Art. 6 Abs. 1 S. 1 lit. b) DSGVO).
          </span>
        </li>
      </ul>
      <h2 id="m15">Änderung und Aktualisierung der Datenschutzerklärung</h2>
      <p>
        Wir bitten Sie, sich regelmäßig über den Inhalt unserer
        Datenschutzerklärung zu informieren. Wir passen die Datenschutzerklärung
        an, sobald die Änderungen der von uns durchgeführten Datenverarbeitungen
        dies erforderlich machen. Wir informieren Sie, sobald durch die
        Änderungen eine Mitwirkungshandlung Ihrerseits (z.B. Einwilligung) oder
        eine sonstige individuelle Benachrichtigung erforderlich wird.
      </p>
      <p>
        Sofern wir in dieser Datenschutzerklärung Adressen und
        Kontaktinformationen von Unternehmen und Organisationen angeben, bitten
        wir zu beachten, dass die Adressen sich über die Zeit ändern können und
        bitten die Angaben vor Kontaktaufnahme zu prüfen.
      </p>
      <p className="seal">
        <a
          href="https://datenschutz-generator.de/"
          title="Rechtstext von Dr. Schwenke - für weitere Informationen bitte anklicken."
          target="_blank"
          rel="noopener noreferrer nofollow"
        >
          Erstellt mit kostenlosem Datenschutz-Generator.de von Dr. Thomas
          Schwenke
        </a>
      </p>
      <Link to={"/home"}>
        <button className="button">Zurück</button>
      </Link>
    </div>
  );
};
