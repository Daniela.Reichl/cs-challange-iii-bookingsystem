import React, { useEffect, useState } from "react";
import heart from "../../../../assets/icons/heart.svg";
import heart_no_fill from "../../../../assets/icons/heart-no-fill.svg";
import { useCreateFavorite } from "../../../../hooks/post/useCreateFavorite";
import { ErrorMessage } from "../../../common/ErrorMessage";
import { FavouriteButtonProps } from "../../../../types/favouriteType";
import { ApiError } from "../../../../types/commonType";

export const FavouriteButton = ({ desk }: FavouriteButtonProps) => {
  //Mark as favorite
  const { mutate: CreateFavorite, isError, error } = useCreateFavorite();
  const [errorMessage, setErrorMessage] = useState<string>("");
  const [showErrorMessage, setShowErrorMessage] = useState(false);

  const markAsFavorite = async () => {
    try {
      await CreateFavorite({
        desk: desk.id,
      });
    } catch (error: any) {
      setErrorMessage(
        "Es ist ein Fehler aufgetreten, versuche es später erneut!"
      );
    }
  };

  //Error handling
  useEffect(() => {
    if (isError && error) {
      if ((error as ApiError).response.status === 400) {
        setErrorMessage(
          "Der Tisch konnte nicht als Favorit markiert werden. Die Eingabedaten sind ungültig."
        );
      } else if ((error as ApiError).response.status === 403) {
        setErrorMessage(
          "Die User Id im JWT und in der Anfrage stimmt nicht überein."
        );
      } else if ((error as ApiError).response.status === 409) {
        setErrorMessage("Dieser Tisch ist bereits ein Favorit.");
      } else {
        setErrorMessage(
          "Es ist ein Fehler aufgetreten, versuche es später erneut!"
        );
      }
      // Show the error message
      setShowErrorMessage(true);

      // Set a timer to hide the error message after 3 seconds
      const timer = setTimeout(() => {
        setShowErrorMessage(false);
        setErrorMessage(""); // Clear the error message after hiding it
      }, 2000);

      // Clean up the timer when the component unmounts or when the error state changes
      return () => clearTimeout(timer);
    }
  }, [isError, error]);

  return (
    //Favourite button
    <button
      onClick={markAsFavorite}
      className={
        desk.fixdesk ? "hidden" : "text-base font-bold text-sandyBrown"
      }
    >
      {/* Button if user favourite  */}
      {desk.isUserFavourite ? (
        <div className="flex gap-2 justify-center items-center">
          {/* Heart icon */}
          <img
            src={heart}
            className="w-8"
            alt="Heart-Icon"
            title="Heart-Icon"
          />
          {/* Text */}
          <p>Favorit</p>
        </div>
      ) : (
        //Button if not user favourite
        <div className="flex gap-2 justify-center items-center">
          {/* Heart icon without fill */}
          <img
            src={heart_no_fill}
            className="w-8"
            alt="Heart-Icon"
            title="Heart-Icon"
          />
          {/* Text */}
          <p>Als Favorit markieren</p>
        </div>
      )}
      {/* Error Message */}
      {showErrorMessage && <ErrorMessage message={errorMessage} />}
    </button>
  );
};
