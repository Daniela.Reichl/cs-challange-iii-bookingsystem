import { useParams } from "react-router-dom";
import { useGetAllUserFavourites } from "../../../../hooks/get/useGetAllUserFavourites";
import { Loader } from "../../../common/Loader";
import { ErrorMessage } from "../../../common/ErrorMessage";
import { FavouriteCard } from "./FavouriteCard";
import { BackButton } from "../../../common/BackButton";
import { Message } from "../../../common/Message";
import { FavouriteViewProps } from "../../../../types/favouriteType";

export const FavouriteView = () => {
  //Get user id from link
  const { id: userId } = useParams();

  //Get all favourites of a user
  const {
    data: favouriteData,
    isLoading,
    isError,
  } = useGetAllUserFavourites(userId!);

  return (
    <div className="baseMain">
      {/* Show all favourites of a user */}
      <div className="baseContent">
        <h2>Meine Favoriten</h2>
        <div className="grid grid-cols-1 lg:grid-cols-2 2xl:grid-cols-3 gap-6">
          {isLoading ? (
            <Loader />
          ) : isError ? (
            <ErrorMessage
              message={
                "Authentifizierung ungültig. Du hast hierfür keine Berechtigung."
              }
            />
          ) : //Message if user has no favourites
          favouriteData?.length === 0 ? (
            <Message message={"Du hast noch keine Favoriten markiert."} />
          ) : (
            //Favourites array
            favouriteData.map((favourite: FavouriteViewProps) => (
              <FavouriteCard key={favourite.id} favourite={favourite} />
            ))
          )}
        </div>
      </div>
      {/* Back button */}
      <BackButton />
    </div>
  );
};
