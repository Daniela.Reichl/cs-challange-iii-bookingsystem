import { useEffect, useState } from "react";
import { BookingButtons } from "../../../common/BookingButtons";
import deleteIcon from "../../../../assets/icons/delete.svg";
import { Popup } from "../../../common/Popup";
import { useDeleteFavourite } from "../../../../hooks/delete/useDeleteFavourite";
import { ErrorMessage } from "../../../common/ErrorMessage";
import { FavouriteCardProps } from "../../../../types/favouriteType";
import { ApiError } from "../../../../types/commonType";

export const FavouriteCard = ({ favourite }: FavouriteCardProps) => {
  //delete favourite
  const [showDeletePopup, setShowDeletePopup] = useState(false);
  const [errorMessage, setErrorMessage] = useState<string>("");

  const {
    mutate: deletedData,
    isError,
    error: deleteError,
  } = useDeleteFavourite();

  const deleteFavourite = async () => {
    try {
      await deletedData(favourite.id);
      handlePopupClose();
    } catch (error: any) {
      setErrorMessage(
        "Es ist ein Fehler aufgetreten, versuche es später erneut!"
      );
    }
  };

  //Error handling
  useEffect(() => {
    if (isError && deleteError) {
      if ((deleteError as ApiError).response.status === 400) {
        setErrorMessage(
          "Favorit wurde nicht gelöscht. Die Eingabedaten sind ungültig."
        );
      } else if ((deleteError as ApiError).response.status === 403) {
        setErrorMessage(
          "Die User Id im JWT und in der Anfrage stimmt nicht überein."
        );
      } else {
        setErrorMessage(
          "Es ist ein Fehler aufgetreten, versuche es später erneut!"
        );
      }
    }
  }, [isError, deleteError]);

  //Close pop-up
  const handlePopupClose = () => {
    setShowDeletePopup(false);
  };

  return (
    <div className="AdminBaseGrid flex flex-col gap-4 justify-start items-start p-5 md:p-10">
      <div className="flex justify-between items-center w-full">
        {/* Desk label */}
        <h4>{favourite.desk.label}</h4>
        {/* Delete button */}
        <button
          onClick={() => setShowDeletePopup(true)}
          className="icon-button"
        >
          <img
            src={deleteIcon}
            alt="Delete Icon"
            title="Delete Icon"
            className="w-6"
          />
        </button>
        {/* Delete pop-up */}
        {showDeletePopup && (
          <Popup
            message={`Soll ${favourite.desk.label} wirklich als Favorit gelöscht werden?`}
            handleSaveClick={deleteFavourite}
            onClose={handlePopupClose}
          />
        )}
      </div>
      {/* Office name */}
      <div>
        <p className="label">Büro</p>
        <p className="capitalize">{favourite.desk.office.name}</p>
      </div>
      {/* Equipment array */}
      <div className="h-24">
        <p className="label">Ausstattung</p>
        {/* Message if desk has no equipment */}
        {favourite.desk.equipment.length === 0 ? (
          <p>Keine Ausstattung vorhanden</p>
        ) : (
          //Equipment list of desk
          <ul className="list-disc pl-5 grid grid-cols-2 gap-x-16">
            {favourite.desk.equipment.map(
              (equipment: string, index: number) => (
                <li key={index} className="capitalize">
                  {equipment}
                </li>
              )
            )}
          </ul>
        )}
      </div>
      {/* Bookings buttons */}
      <div className="flex justify-center w-full">
        <BookingButtons
          deskId={favourite.desk.id}
          deskLabel={favourite.desk.label}
        />
      </div>
      {/* ErrorMessage */}
      <ErrorMessage message={errorMessage} />
    </div>
  );
};
