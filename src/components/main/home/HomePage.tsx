import { Link } from "react-router-dom";
import { useGetLoggedInUser } from "../../../hooks/get/useGetLoggedInUser";
import heart from "../../../assets/icons/heart.svg";
import book from "../../../assets/icons/book.svg";
import user from "../../../assets/icons/user.svg";
import desk from "../../../assets/icons/desk.svg";
import admin from "../../../assets/icons/admin.svg";
import { Loader } from "../../common/Loader";
import { ErrorMessage } from "../../common/ErrorMessage";

export const HomePage = () => {
  //Get logged in user for welcome and admin check
  const { data: loggedInUser, isLoading, isError } = useGetLoggedInUser();

  return (
    <div className="baseMain">
      {isLoading ? (
        <Loader />
      ) : isError ? (
        <ErrorMessage
          message={
            "Authentifizierung ungültig. Du hast hierfür keine Berechtigung."
          }
        />
      ) : loggedInUser ? (
        //Home grid
        <div className="baseContent">
          {/* Welcome user */}
          <h2 className="text-center">
            Hallo, <span className="capitalize">{loggedInUser.firstname}</span>!{" "}
          </h2>
          <div className="grid grid-cols-1 md:grid-cols-2 gap-4 w-72 md:w-auto">
            {/* Link Favoriten */}
            <Link to={`/favourites/${loggedInUser.id}`} className="adminGrid">
              <img
                src={heart}
                alt="Heart-Icon"
                title="Heart-Icon"
                className="w-16"
              />
              <h3>Meine Favoriten</h3>
            </Link>
            {/* Link Reservierungen */}
            <Link
              to={`/user/bookings/${loggedInUser.id}`}
              className="adminGrid"
            >
              <img src={book} alt="Book-Icon" title="Book-Icon" />
              <h3>Meine Buchungen</h3>
            </Link>
            {/* Link Tischbuchung */}
            <Link
              to="/book/office-overview"
              state={{ linkTags: "/book/office-bookingplan" }}
              className="adminGrid"
            >
              <img src={desk} alt="Desk-Icon" title="Desk-Icon" />
              <h3>Tisch buchen</h3>
            </Link>
            {/* Link Profil */}
            <Link to={`/user/profile/${loggedInUser.id}`} className="adminGrid">
              <img src={user} alt="User-Icon" title="User-Icon" />
              <h3>Mein Profil</h3>
            </Link>
            {/* Link Administration */}
            <Link
              to="/admin"
              className={
                loggedInUser.isAdmin
                  ? "adminGrid col-span-1 md:col-span-2"
                  : "hidden"
              }
            >
              <img
                src={admin}
                alt="Admin-Icon"
                title="Admin-Icon"
                className="w-16"
              />
              <h3>Administration</h3>
            </Link>
          </div>
        </div>
      ) : null}
    </div>
  );
};
