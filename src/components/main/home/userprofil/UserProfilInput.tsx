import React, { useState } from "react";
import {
  UserProfilInputProps,
  UserProfileInput,
} from "../../../../types/userType";
import { useGetLoggedInUser } from "../../../../hooks/get/useGetLoggedInUser";
import { Loader } from "../../../common/Loader";
import { useUpdateUser } from "../../../../hooks/put/useUpdateUser";
import { useNavigate, useParams } from "react-router-dom";
import { getLoginToken } from "../../../../config/config";
import { useQueryClient } from "react-query";
import { useGetAllDepartments } from "../../../../hooks/get/useGetAllDepartments";
import { ErrorMessage } from "../../../common/ErrorMessage";
import { InputFieldPassword } from "../../../common/InputFieldPassword";
import { InputFieldText } from "../../../common/InputFieldText";
import { InputFieldSelectDepartments } from "../../../common/InputFieldSelect";

export const UserProfilInput = ({
  editing,
  setEditing,
}: UserProfilInputProps) => {
  const navigate = useNavigate();
  const [errorMessage, setErrorMessage] = useState<string>("");

  const { data: loggedInUserData } = useGetLoggedInUser();
  const { data: departments } = useGetAllDepartments();

  const queryClient = useQueryClient();

  //   update user
  const { id } = useParams<{ id: string }>();
  const token = getLoginToken();
  const updateUserMutation = useUpdateUser(id!, token!);

  const [userData, setUserData] = useState<UserProfileInput>(
    loggedInUserData ?? {}
  );

  if (loggedInUserData === undefined || departments === undefined) {
    return <Loader />;
  }

  // Handle input change in edit mode
  const handleInputChange = (
    event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) => {
    const { name, value } = event.target;
    setUserData((prevUserData) => ({
      ...prevUserData,
      [name]: value,
    }));
  };

  // save updated user data
  const handleSaveClick = async (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    event.preventDefault();
    try {
      // update user Datas
      await updateUserMutation.mutateAsync(userData);
      setEditing(false);
      queryClient.invalidateQueries("useGetLoggedInUser");
    } catch (error: any) {
      if (error.status === 400) {
        // Input data invalid
        setErrorMessage("Eingabedaten ungültig");
      } else if (error.status === 403) {
        // The user associated with the userId in the token you sent does not have permission to access this route.
        setErrorMessage("Sie haben hierfür keine Berechtigung");
      } else if (error.status === 404) {
        //User with the given deskId not found.
        setErrorMessage("Es wurde kein Tisch mit dieser ID gefunden");
      } else if (error.status === 409) {
        // Error updating user data: A user with this email already exists
        setErrorMessage("Die E-Mail-Adresse ist bereits vergeben");
      } else {
        // Show any other error
        setErrorMessage(
          "Es ist ein Fehler aufgetreten, versuche es später wieder"
        );
      }
    }
  };

  //   logout user
  const handleLogout = () => {
    localStorage.removeItem("loginToken");
    localStorage.removeItem("refreshToken");
    navigate("/");
    window.location.reload();
  };

  // handle cancel input
  const handleCancelClick = () => {
    setUserData(loggedInUserData ?? {});
    setEditing(false);
  };

  return (
    <form className="flex flex-col gap-5 w-full">
      <InputFieldText
        label="Vorname"
        name="firstname"
        value={userData.firstname || loggedInUserData.firstname}
        editing={editing}
        onChange={handleInputChange}
      />
      <InputFieldText
        label="Nachname"
        name="lastname"
        value={userData.lastname || loggedInUserData.lastname}
        editing={editing}
        onChange={handleInputChange}
      />
      <InputFieldSelectDepartments
        label="Firma"
        name="department"
        value={userData.department || loggedInUserData?.department}
        editing={editing}
        options={departments}
        onChange={handleInputChange}
      />
      <InputFieldText
        label="E-Mail"
        name="email"
        value={userData.email || loggedInUserData.email}
        editing={editing}
        onChange={handleInputChange}
      />
      <InputFieldPassword
        label="Passwort"
        name="password"
        value={
          editing
            ? userData.password || loggedInUserData?.password
            : "*********"
        }
        editing={editing}
        onChange={handleInputChange}
      />
      {/* if editing, display this section */}
      {editing && (
        <div className="flex gap-5 justify-center">
          <button className="button" onClick={handleCancelClick}>
            Abbrechen
          </button>
          <button className="button" onClick={handleSaveClick}>
            Speichern
          </button>
        </div>
      )}
      {/* only show logout if user is not editing*/}
      {!editing && (
        <button className="button self-center" onClick={handleLogout}>
          Logout
        </button>
      )}
      <ErrorMessage message={errorMessage} />
    </form>
  );
};
