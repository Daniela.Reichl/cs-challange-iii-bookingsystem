import { useState } from "react";
import { BackButton } from "../../../common/BackButton";
import edit from "/src/assets/icons/edit.svg";
import { UserProfilInput } from "./UserProfilInput";

export const UserProfileView = () => {
  const [editing, setEditing] = useState(false);

  // open Edit input
  const handleEditClick = () => {
    setEditing(true);
  };

  return (
    <div className="baseMain">
      <div className="baseContent w-72 md:w-96">
        <div className="flex gap-5">
          <h2>Profil</h2>
          <button onClick={handleEditClick} className="icon-button">
            <img className="w-8" src={edit} alt="edit" />
          </button>
        </div>
        <UserProfilInput editing={editing} setEditing={setEditing} />
      </div>
      <BackButton />
    </div>
  );
};
