import { useParams } from "react-router-dom";
import { DeskBookingPlan } from "./DeskBookingPlan";
import { useGetOfficeById } from "../../../../hooks/get/useGetOfficeById";
import { Loader } from "../../../common/Loader";
import { ErrorMessage } from "../../../common/ErrorMessage";
import { BackButton } from "../../../common/BackButton";

export const OfficeBookingPlan = () => {
  //Get office id from link
  const { id } = useParams();

  //Get office by id
  const { data: officeData, isLoading, isError } = useGetOfficeById(id!);

  return (
    //Office booking plan
    <div className="baseMain">
      <div className="baseContent">
        {isLoading ? (
          <Loader />
        ) : isError ? (
          <ErrorMessage
            message={
              "Authentifizierung ungültig. Du hast hierfür keine Berechtigung."
            }
          />
        ) : officeData ? (
          <div className="flex flex-col gap-10 w-full">
            {/* Office name */}
            <h2 className="capitalize text-center">{officeData.name}</h2>
            {/* Show all desks */}
            <DeskBookingPlan officeId={id!} />
          </div>
        ) : null}
      </div>
      {/* Back button */}
      <BackButton />
    </div>
  );
};
