import { useGetAllDesks } from "../../../../hooks/get/useGetAllDesks";
import { Loader } from "../../../common/Loader";
import { ErrorMessage } from "../../../common/ErrorMessage";
import { DeskElement } from "./DeskElement";
import { Message } from "../../../common/Message";
import { DeskBookingProps, DeskProps } from "../../../../types/deskType";

export const DeskBookingPlan = ({ officeId }: DeskBookingProps) => {
  //Get all desks
  const {
    data: deskData,
    isLoading: deskDataLoading,
    isError: deskDataError,
  } = useGetAllDesks();

  // loading & error desk data
  if (deskDataLoading) {
    return <Loader />;
  } else if (deskDataError) {
    return (
      <ErrorMessage
        message={
          "Authentifizierung ungültig. Du hast hierfür keine Berechtigung."
        }
      />
    );
  }

  //show only desks of office chosen
  const filteredDeskData = deskData?.filter(
    (desk: DeskProps) => desk.office.id === officeId
  );

  return (
    <div className="flex flex-col gap-6">
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4 w-full">
        {/* Filtered desk data array to display single desks in office */}
        {filteredDeskData.length > 0 ? (
          filteredDeskData.map((desks: DeskProps) => (
            <DeskElement key={desks.id} desk={desks} />
          ))
        ) : (
          <div className="col-span-3 flex justify-center">
            {/* Message if no desks are available */}
            <Message
              message={
                "In diesem Büro sind derzeit keine Tische zur Buchung verfügbar."
              }
            />
          </div>
        )}
      </div>
      {/* Legende */}
      <div className={filteredDeskData.length === 0 ? "hidden" : "flex gap-6"}>
        {/* Flex desk */}
        <div className="flex gap-2 justify-center items-center">
          <div className="bg-standardViolet w-8 h-8 rounded-md"></div>
          <p>Flex Desk</p>
        </div>
        {/* Fix desk */}
        <div className="flex gap-2 justify-center items-center">
          <div className="bg-lightGrey inner-border-2 inner-border-standardViolet w-8 h-8 rounded-md"></div>
          <p>Fix Desk</p>
        </div>
      </div>
    </div>
  );
};
