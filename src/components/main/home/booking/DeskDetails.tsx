import close from "/src/assets/icons/close-details.svg";
import { useGetDeskById } from "../../../../hooks/get/useGetDeskById";
import { Loader } from "../../../common/Loader";
import { ErrorMessage } from "../../../common/ErrorMessage";
import { DeskDetailsProps } from "../../../../types/deskType";
import { BookingProps, BookingSortProps } from "../../../../types/bookingType";
import { BookingButtons } from "../../../common/BookingButtons";

export const DeskDetails = ({ deskId, onClose }: DeskDetailsProps) => {
  //Format Date
  const formatDate = (dateString: string) => {
    const date = new Date(dateString);
    const day = date.getDate().toString().padStart(2, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const year = date.getFullYear().toString();
    return `${day}.${month}.${year}`;
  };

  //Get desk by ID
  const { data: deskData, isLoading, isError } = useGetDeskById(deskId);

  return (
    //Desk details popup
    <div className="h-full w-full fixed top-0 left-0 flex justify-center items-center z-10 bg-black bg-opacity-30 ">
      {isLoading ? (
        <Loader />
      ) : isError ? (
        <ErrorMessage
          message={
            "Authentifizierung ungültig. Du hast hierfür keine Berechtigung."
          }
        />
      ) : deskData ? (
        <div className="bg-white flex flex-col gap-4 justify-center items-start p-5 md:p-10 rounded-md shadow-md w-[90%] md:w-[55%] lg:w-[36%] 2xl:w-1/4">
          <div className="flex justify-between items-center w-full">
            {/* Desk label */}
            <h4 className="capitalize">{deskData.label}</h4>
            {/* Close button */}
            <button className="icon-button" onClick={onClose}>
              <img
                src={close}
                alt="Close-Icon"
                title="Close-Icon"
                className="w-6"
              />
            </button>
          </div>
          {/* Office name */}
          <div>
            <p className="label">Büro</p>
            <p className="capitalize">{deskData.office.name}</p>
          </div>
          {/* Equipment of desk */}
          <div>
            <p className="label">Ausstattung</p>
            {/* Message if desk has no equipment */}
            {deskData.equipment.length === 0 ? (
              <p>Keine Ausstattung vorhanden</p>
            ) : (
              //Equipment array
              <ul className="list-disc pl-10">
                {deskData.equipment.map((equipment: string, index: number) => (
                  <li key={index} className="capitalize">
                    {equipment}
                  </li>
                ))}
              </ul>
            )}
          </div>
          {/* Next Bookings */}
          <div className="">
            <p className="label">Nächste Buchungen:</p>
            {/* Message if desk has no next bookings */}
            {deskData.bookings.length === 0 ? (
              <p>keine Buchungen vorhanden</p>
            ) : (
              //Next bookings array
              <ul className="list-disc pl-10 overflow-y-auto max-h-36 pr-4 scrollbar-thin scrollbar-thumb-darkViolet scrollbar-track-transparent">
                {deskData.bookings
                  //Sort next bookings by start date
                  .slice()
                  .sort(
                    (a: BookingSortProps, b: BookingSortProps) =>
                      Date.parse(a.dateStart) - Date.parse(b.dateStart)
                  )
                  //array
                  .map((booking: BookingProps, index: number) => (
                    <li key={index}>
                      {formatDate(booking.dateStart)} -{" "}
                      {formatDate(booking.dateEnd)}
                    </li>
                  ))}
              </ul>
            )}
          </div>
          {/* Fix desk/flex desk booking buttons */}
          <BookingButtons deskId={deskData.id} deskLabel={deskData.label} />
        </div>
      ) : null}
    </div>
  );
};
