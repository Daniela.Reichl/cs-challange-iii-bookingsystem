import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { useGetLoggedInUser } from "../../../../../hooks/get/useGetLoggedInUser";
import { useCreateFixdesk } from "../../../../../hooks/post/useCreateFixdesk";
import { ApiError } from "../../../../../types/commonType";
import { Loader } from "../../../../common/Loader";
import { ErrorMessage } from "../../../../common/ErrorMessage";
import { Popup } from "../../../../common/Popup";
import { BackButton } from "../../../../common/BackButton";

export const FixDeskRequestSummary = () => {
  //Get desk id and desk label from link
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const deskId = queryParams.get("deskId");
  const deskLabel = queryParams.get("deskLabel");

  //set desk label as input value
  const [inputValue, setInputValue] = useState("");

  useEffect(() => {
    setInputValue(deskLabel || "");
  }, []);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value);
  };

  //Get firstname, lastname & email of logged in user
  const {
    data: loggedInUser,
    isLoading,
    isError: loggedInUserError,
  } = useGetLoggedInUser();

  //Fix desk Request
  const { mutate: CreateFixdesk, isError, error } = useCreateFixdesk();
  const [errorMessage, setErrorMessage] = useState<string>("");
  const [showPopup, setShowPopup] = useState(false);

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setShowPopup(true);
  };

  const handleFixdeskRequest = async () => {
    try {
      await CreateFixdesk({
        desk: deskId!,
      });
      handlePopupClose();
    } catch (error: any) {
      const status = error.response.status;
      if (status === 400) {
        // 	Input data invalil
        setErrorMessage("Input data invalid");
      } else if (status === 403) {
        // The userIds in the JWT and the request don't match.
        setErrorMessage("The userIds in the JWT and the request don't match.");
      } else {
        // Handle other error status codes
        setErrorMessage("An error occurred. Please try again later.");
      }
    }
  };

  //Error handling
  useEffect(() => {
    if (isError && error) {
      if ((error as ApiError).response.status === 400) {
        setErrorMessage(
          "Die Anfrage wurde nicht erstellt. Die Eingabedaten sind ungültig."
        );
      } else if ((error as ApiError).response.status === 403) {
        setErrorMessage(
          "Die User Id im JWT und in der Anfrage stimmt nicht überein."
        );
      } else if ((error as ApiError).response.status === 409) {
        setErrorMessage(
          "Die Anfrage wurde nicht erstellt. Der Benutzer hat bereits eine Fixdesk-Anfrage erstellt."
        );
      } else {
        setErrorMessage(
          "Es ist ein Fehler aufgetreten, versuche es später erneut!"
        );
      }
    }
  }, [isError, error]);

  //close Popup
  const handlePopupClose = () => {
    setShowPopup(false);
  };

  return (
    <div className="baseMain">
      <div className="baseContent md:w-[80%] lg:w-1/2 2xl:w-1/3">
        <h2>Fixdesk Anfrage</h2>
        {/* Fix desk request form */}
        <form
          onSubmit={handleSubmit}
          className="flex flex-col gap-4 items-center"
        >
          {isLoading ? (
            <Loader />
          ) : loggedInUserError ? (
            <ErrorMessage
              message={
                "Authentifizierung ungültig. Du hast hierfür keine Berechtigung."
              }
            />
          ) : loggedInUser ? (
            <div className="flex flex-col gap-4 items-center">
              {/* Firstname & lastname input*/}
              <input
                type="text"
                value={loggedInUser.firstname + " " + loggedInUser.lastname}
                className="input capitalize"
                disabled
              />
              {/* Email address input */}
              <input
                type="text"
                value={loggedInUser.email}
                className="input"
                disabled
              />
            </div>
          ) : null}
          {/* Desk label input */}
          <input
            type="text"
            value={inputValue}
            onChange={handleChange}
            className="input"
            disabled
          />
          {/* Info message */}
          <p>
            Bitte beachte, dass keine Flex Desks mehr gebucht werden können,
            sobald deine Fix Desk Anfrage bestätigt wurde.
          </p>
          {/* Submit button */}
          <button type="submit" className="button mt-5">
            Anfragen
          </button>
          {/* Booking pop-up */}
          {showPopup && (
            <Popup
              message={`Soll ${deskLabel} wirklich als Fixdesk angefragt werden?`}
              handleSaveClick={handleFixdeskRequest}
              onClose={handlePopupClose}
            />
          )}
        </form>
        {/* API ErrorMessage */}
        {errorMessage && <ErrorMessage message={errorMessage} />}
      </div>
      {/* Back button */}
      <BackButton />
    </div>
  );
};
