import { Link } from "react-router-dom";

export const FixdeskSuccess = () => {
  return (
    <div className="baseMain">
      <div className="baseContent">
        {/* Party icon */}
        <img src="../src/assets/icons/party-horn.svg" alt="party-icon" />
        {/* Title */}
        <h2 className="text-center">Fixdesk Anfrage erfolgreich</h2>
        {/* Text */}
        <p className="w-full lg:w-1/2 text-center">
          Deine Fixdesk Anfrage wurde erfolgreich an den Admin übermittelt. Den
          Status kannst du jederzeit in der Reservierungsübersicht einsehen.
        </p>
        {/* Back home button */}
        <button className="buttonFit">
          <Link to="/home"> Zurück zur Homepage</Link>
        </button>
      </div>
    </div>
  );
};
