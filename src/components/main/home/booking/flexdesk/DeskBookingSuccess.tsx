import { Link } from "react-router-dom";

export const DeskBookingSuccess = () => {
  return (
    <div className="baseMain">
      <div className="baseContent">
        {/* Party icon */}
        <img src="../src/assets/icons/party-horn.svg" alt="party-icon" />
        {/* Title */}
        <h2>Buchung erfolgreich</h2>
        {/* Text */}
        <p>
          Danke für deine Buchung! Eine Übersicht zu deinen Buchungen findest du
          unter "Meine Buchungen".
        </p>
        {/* Back home button */}
        <button className="buttonFit">
          <Link to="/home"> Zurück zur Homepage</Link>
        </button>
      </div>
    </div>
  );
};
