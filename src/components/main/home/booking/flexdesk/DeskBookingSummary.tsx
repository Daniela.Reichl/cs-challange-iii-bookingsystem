import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useGetLoggedInUser } from "../../../../../hooks/get/useGetLoggedInUser";
import {
  CreateBookingProps,
  FlexDeskBookingInput,
} from "../../../../../types/bookingType";
import { useCreateBooking } from "../../../../../hooks/post/useCreateBooking";
import { ApiError } from "../../../../../types/commonType";
import { ErrorMessage } from "../../../../common/ErrorMessage";
import { Loader } from "../../../../common/Loader";
import { Popup } from "../../../../common/Popup";
import { BackButton } from "../../../../common/BackButton";

//yup validation
const schema = yup.object().shape({
  dateStart: yup.string().required(),
  dateEnd: yup.string().required(),
});

export const DeskBookingSummary = () => {
  //Get desk id and desk label from link
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const deskId = queryParams.get("deskId");
  const deskLabel = queryParams.get("deskLabel");

  //set desk label as input value
  const [inputValue, setInputValue] = useState("");

  useEffect(() => {
    setInputValue(deskLabel || "");
  }, []);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value);
  };

  //Get firstname, lastname & email of logged in user
  const {
    data: loggedInUser,
    isLoading,
    isError: loggedInUserError,
  } = useGetLoggedInUser();

  //React-hook form
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<FlexDeskBookingInput>({ resolver: yupResolver(schema) });

  //Flex desk Request
  const { mutate: CreateBooking, error, isError } = useCreateBooking();
  const [errorMessage, setErrorMessage] = useState<string>("");
  const [flexBooking, setFlexBooking] = useState<CreateBookingProps>({
    dateStart: "",
    dateEnd: "",
    desk: "",
  });
  const [showPopup, setShowPopup] = useState(false);

  const onSubmit = (formData: FlexDeskBookingInput) => {
    const newFlexBooking = {
      dateStart: formData.dateStart,
      dateEnd: formData.dateEnd,
      desk: deskId!,
    };
    setFlexBooking(newFlexBooking);
    setShowPopup(true);
  };

  const handleFlexdeskRequest = async () => {
    try {
      await CreateBooking(flexBooking);
      reset();
      handlePopupClose();
    } catch (error: any) {
      if (error) {
        setErrorMessage(
          "Es ist ein Fehler aufgetreten, versuche es später erneut!"
        );
      }
    }
  };

  //Error handling
  useEffect(() => {
    if (isError && error) {
      if ((error as ApiError).response.status === 400) {
        setErrorMessage(
          "Die Buchung konnte nicht erstellt werden. Die Buchung ist entweder zu lange oder liegt nicht im gültigen Bereich von 4 Wochen."
        );
      } else if ((error as ApiError).response.status === 403) {
        setErrorMessage(
          "Die User Id im JWT und in der Anfrage stimmt nicht überein."
        );
      } else if ((error as ApiError).response.status === 406) {
        setErrorMessage(
          "Die Buchung konnte nicht erstellt werden. Es existiert eine Fixdesk-Buchung für diesen Tisch."
        );
      } else if ((error as ApiError).response.status === 409) {
        setErrorMessage(
          "Die Buchung konnte nicht erstellt werden. Die Daten überschneiden sich mit anderen Buchungen (entweder deine eigene Buchung für einen anderen Tisch oder andere Buchungen für diesen Tisch)."
        );
      } else {
        setErrorMessage(
          "Es ist ein Fehler aufgetreten, versuche es später erneut!"
        );
      }
    }
  }, [isError, error]);

  //close Popup
  const handlePopupClose = () => {
    setShowPopup(false);
  };

  return (
    <div className="baseMain">
      <div className="baseContent md:w-[80%] lg:w-1/2 2xl:w-1/3">
        <h2>Buchungszusammenfassung</h2>
        {/* Flex desk request form */}
        <form
          onSubmit={handleSubmit(onSubmit)}
          className="flex flex-col gap-4 items-center"
        >
          {isLoading ? (
            <Loader />
          ) : loggedInUserError ? (
            <ErrorMessage
              message={
                "Authentifizierung ungültig. Du hast hierfür keine Berechtigung."
              }
            />
          ) : loggedInUser ? (
            <div className="flex flex-col gap-4 items-center">
              {/* Firstname & lastname input*/}
              <input
                type="text"
                value={loggedInUser.firstname + " " + loggedInUser.lastname}
                className="input capitalize"
                disabled
              />
              {/* Email address input */}
              <input
                type="text"
                value={loggedInUser.email}
                className="input"
                disabled
              />
            </div>
          ) : null}
          {/* Desk label input */}
          <input
            type="text"
            value={inputValue}
            onChange={handleChange}
            className="input"
            disabled
          />
          {/* Start date input */}
          <div className="flex flex-col">
            <label htmlFor="dateStart" className="label">
              Startdatum
            </label>
            <input
              type="date"
              id="dateStart"
              className="input"
              {...register("dateStart")}
            />
            {/* Start date error message */}
            {errors.dateStart && (
              <div>
                <ErrorMessage message="Bitte gib ein Startdatum ein!" />
              </div>
            )}
          </div>
          {/* End date input */}
          <div className="flex flex-col">
            <label htmlFor="dateEnd" className="label">
              Enddatum
            </label>
            <input
              type="date"
              id="dateEnd"
              className="input"
              {...register("dateEnd")}
            />
            {/* End date error message */}
            {errors.dateEnd && (
              <div>
                <ErrorMessage message="Bitte gib ein Enddatum ein!" />
              </div>
            )}
          </div>
          {/* Info message */}
          <p>
            Bitte beachte, dass Flex Desks nur max. eine Woche (Montag -
            Freitag, nicht über das Wochenende) gebucht werden können.
          </p>
          {/* Submit button */}
          <button className="button">Buchen</button>
          {/* Booking pop-up */}
          {showPopup && (
            <Popup
              message={`Möchtest du ${deskLabel} wirklich buchen?`}
              handleSaveClick={handleFlexdeskRequest}
              onClose={handlePopupClose}
            />
          )}
        </form>
        {/* API ErrorMessage */}
        {errorMessage && <ErrorMessage message={errorMessage} />}
      </div>
      {/* Back button */}
      <BackButton />
    </div>
  );
};
