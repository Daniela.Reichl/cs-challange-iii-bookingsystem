import { Link } from "react-router-dom";
import location from "/src/assets/icons/location.svg";
import { OfficeElementProps } from "../../../../types/officeType";

export const OfficeElement = ({ office, linkTag }: OfficeElementProps) => {
  return (
    <div>
      {/* Link to single office */}
      <Link
        to={`${linkTag}/${office.id}`}
        className="buttonFit flex gap-2 justify-start items-center"
      >
        {/* Location icon */}
        <img
          src={location}
          alt="Location-Icon"
          title="Location-Icon"
          className="w-8"
        />
        {/* Office name */}
        <p className="capitalize">{office.name}</p>
      </Link>
    </div>
  );
};
