import { useState } from "react";
import { DeskDetails } from "./DeskDetails";
import { DeskElementProps } from "../../../../types/deskType";
import { FavouriteButton } from "../favourites/FavouriteButton";

export const DeskElement = ({ desk }: DeskElementProps) => {
  //Format date
  const formatDate = (dateString: string) => {
    const date = new Date(dateString);
    const day = date.getDate().toString().padStart(2, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const year = date.getFullYear().toString();
    return `${day}.${month}.${year}`;
  };

  //Show details of a desk
  const [deskId, setDeskId] = useState<string>("");
  const [showDetails, setShowDetails] = useState<boolean>(false);

  const showDeskDetails = () => {
    setDeskId(desk.id);
    setShowDetails(true);
  };

  const closeDeskDetails = () => {
    setShowDetails(false);
  };

  return (
    <div>
      {/* Single Desk */}
      <div
        className={
          desk.fixdesk
            ? "bg-lightGrey inner-border-2 inner-border-standardViolet px-4 py-2 cursor-not-allowed flex flex-col justify-center items-center rounded-md w-full h-44"
            : "desk cursor-pointer px-4 py-2 flex flex-col gap-6 justify-center items-center w-full h-44 object-cover"
        }
      >
        {/* Fixdesk onClick not possible */}
        <div onClick={desk.fixdesk ? undefined : showDeskDetails}>
          {/* Desk label */}
          <h4 className="capitalize text-center">{desk.label}</h4>
          {/* Show if there is next booking */}
          {desk.nextBooking ? (
            <p className={desk.fixdesk ? "hidden" : "block font-bold"}>
              Nächste Buchung: {formatDate(desk.nextBooking)}
            </p>
          ) : (
            //Message if desk has no next booking
            <p className={desk.fixdesk ? "hidden" : "block font-bold"}>
              Keine Buchung in nächster Zeit
            </p>
          )}
        </div>
        {/* Favourite button */}
        <FavouriteButton desk={desk} />
      </div>
      {/* Show details popup */}
      {showDetails && (
        <DeskDetails deskId={deskId} onClose={closeDeskDetails} />
      )}
    </div>
  );
};
