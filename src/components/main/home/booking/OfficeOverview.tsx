import { useLocation } from "react-router-dom";
import { OfficeElement } from "./OfficeElement";
import { useGetAllOffices } from "../../../../hooks/get/useGetAllOffices";
import { Loader } from "../../../common/Loader";
import { ErrorMessage } from "../../../common/ErrorMessage";
import { OfficeProps } from "../../../../types/officeType";
import { Message } from "../../../common/Message";
import { BackButton } from "../../../common/BackButton";

export const OfficeOverview = () => {
  //Get all offices
  const { data: allOffices, isLoading, isError } = useGetAllOffices();

  //Display OfficeOverview in booking and admin section
  const location = useLocation();
  // get state from privious page
  const linkTags = location.state?.linkTags || {};

  return (
    <div className="baseMain">
      <div className="baseContent">
        <h2>Wähle ein Büro aus</h2>
        <div className="grid grid-cols-1 md:grid-cols-2 gap-6">
          {isLoading ? (
            <Loader />
          ) : isError ? (
            <ErrorMessage
              message={
                "Authentifizierung ungültig. Du hast hierfür keine Berechtigung."
              }
            />
          ) : allOffices.length > 0 ? (
            //Office array
            allOffices.map((offices: OfficeProps) => (
              <OfficeElement
                office={offices}
                key={offices.id}
                linkTag={linkTags}
              />
            ))
          ) : (
            //Message if office array is empty
            <Message message={"Derzeit gibt es keine Büros zur Auswahl."} />
          )}
        </div>
      </div>
      {/* Back button */}
      <BackButton />
    </div>
  );
};
