import { FixDeskType } from "../../../../../types/deskType";
import { useAnswerFixdeskRequest } from "../../../../../hooks/put/useAnswerFixdesRequest";
import confirm from "../../../../../assets/icons/confirm.svg";
import close from "../../../../../assets/icons/close-details.svg";

type AdminFixdeskRequestsCardPorps = {
  fixDeskRequest: FixDeskType;
  setErrorMessage: (value: string) => void;
  setMessage: (value: string) => void;
};

export const AdminFixdeskRequestsCard = ({
  fixDeskRequest,
  setErrorMessage,
  setMessage,
}: AdminFixdeskRequestsCardPorps) => {
  const dateString = fixDeskRequest.createdAt;

  // date
  const date = new Date(dateString);
  const answerFixdeskRequest = useAnswerFixdeskRequest();

  const formattedDate = date.toLocaleDateString("de-DE", {
    day: "2-digit",
    month: "2-digit",
    year: "numeric",
  });

  const handelFixdeskApprove = async () => {
    try {
      await answerFixdeskRequest.mutateAsync({
        ...fixDeskRequest,
        status: "approved",
      });
      setMessage("Fix-Desk-Anfrage bestätigt");
    } catch (error: any) {
      if (error.response) {
        if (error === 400) {
          // Input data invalid
          setErrorMessage("Eingabedaten ungültig");
        } else if (error === 403) {
          // The user associated with the userId in the token you sent does not have permission to access this route.
          setErrorMessage("Sie haben hierfür keine Berechtigung");
        } else if (error === 404) {
          //User with the given deskId not found.
          setErrorMessage(
            "Es wurde keine Fix-Desk Anfrage mit dieser ID gefunden"
          );
        } else {
          setErrorMessage(
            "Es ist ein Fehler aufgetreten, versuche es später wieder"
          );
        }
      }
    }
  };

  const handelFixdeskDecline = async () => {
    try {
      await answerFixdeskRequest.mutateAsync({
        ...fixDeskRequest,
        status: "rejected",
      });
      setMessage("Fix-Desk-Anfrage abgelehnt");
    } catch (error: any) {
      if (error.response) {
        if (error === 400) {
          // Input data invalid
          setErrorMessage("Eingabedaten ungültig");
        } else if (error === 403) {
          // The user associated with the userId in the token you sent does not have permission to access this route.
          setErrorMessage("Sie haben hierfür keine Berechtigung");
        } else if (error === 404) {
          //User with the given deskId not found.
          setErrorMessage(
            "Es wurde keine Fix-Desk Anfrage mit dieser ID gefunden"
          );
        } else {
          setErrorMessage(
            "Es ist ein Fehler aufgetreten, versuche es später wieder"
          );
        }
      }
    }
  };

  return (
    <div className="flex flex-col md:flex-row gap-5">
      <div className="bg-white flex flex-col md:flex-row gap-4 justify-between items-center px-5 py-4 rounded-md shadow-md w-72 md:w-[35rem]">
        <div className="grid grid-cols-1 md:grid-cols-2 gap-5">
          <p className="capitalize w-52">
            <span className="label">Von:</span> {fixDeskRequest.user.firstname}{" "}
            {fixDeskRequest.user.lastname}
          </p>
          <p>
            <span className="label">Tisch:</span> {fixDeskRequest.desk.label}
          </p>
          <p>
            <span className="label">Datum:</span> {formattedDate}
          </p>
          <p>
            <span className="label">Status:</span>{" "}
            {fixDeskRequest.status === "approved"
              ? "bestätigt"
              : fixDeskRequest.status === "rejected"
              ? "abgelehnt"
              : fixDeskRequest.status === "notreviewed"
              ? "offen"
              : fixDeskRequest.status}
          </p>
        </div>
        {fixDeskRequest.status === "notreviewed" ? (
          <div className="flex gap-4">
            <button className="icon-button" onClick={handelFixdeskApprove}>
              <img
                src={confirm}
                alt="Confirm-Icon"
                title="Confirm-Icon"
                className="w-8"
              />
            </button>
            <button className="icon-button" onClick={handelFixdeskDecline}>
              <img
                src={close}
                alt="Close-Icon"
                title="Close-Icon"
                className="w-8"
              />
            </button>
          </div>
        ) : null}
      </div>
    </div>
  );
};
