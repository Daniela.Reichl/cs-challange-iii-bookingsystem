import { useState } from "react";
import { useGetAllFixDeskRequest } from "../../../../../hooks/get/useGetAllFixDeskRequest";
import { AdminFixdeskRequestsCard } from "./AdminFixdeskRequestsCard";
import { BackButton } from "../../../../common/BackButton";
import { Loader } from "../../../../common/Loader";
import { ErrorMessage } from "../../../../common/ErrorMessage";
import { Message } from "../../../../common/Message";
import { FixDeskType } from "../../../../../types/deskType";

export const AdminFixdesksRequestView = () => {
  const { data: allFixDeskRequests, isLoading } = useGetAllFixDeskRequest();

  const [message, setMessage] = useState<string>("");
  const [errorMessage, setErrorMessage] = useState<string>("");

  // check is there are requests with status "notreviewed"
  const notReviewedFixDeskRequests = allFixDeskRequests
    ? allFixDeskRequests.filter(
        (fixDeskRequest: FixDeskType) => fixDeskRequest.status === "notreviewed"
      )
    : [];

  // check is there are requests with status "approved" or "rejected"
  const reviewedFixDeskRequests = allFixDeskRequests
    ? allFixDeskRequests.filter(
        (fixDeskRequest: FixDeskType) =>
          fixDeskRequest.status === "approved" ||
          fixDeskRequest.status === "rejected"
      )
    : [];

  // sort reviewedFixDeskRequests by date
  const sortedReviewedFixDeskRequests: FixDeskType[] =
    reviewedFixDeskRequests.sort((a: FixDeskType, b: FixDeskType) => {
      const dateA: number = new Date(a.createdAt).getTime();
      const dateB: number = new Date(b.createdAt).getTime();
      return dateB - dateA;
    });

  return (
    <div className="baseMain">
      <div className="baseContent">
        <h2>Fix-Desk Anfragen</h2>
        <h3 className="self-start">Offene Anfragen</h3>
        <div className="flex flex-col gap-5 w-full">
          {isLoading ? (
            <div>
              <Loader />
            </div>
          ) : notReviewedFixDeskRequests.length > 0 ? (
            notReviewedFixDeskRequests.map((fixDeskRequest: FixDeskType) => (
              <AdminFixdeskRequestsCard
                key={fixDeskRequest.id}
                fixDeskRequest={fixDeskRequest}
                setMessage={setMessage}
                setErrorMessage={setErrorMessage}
              />
            ))
          ) : (
            <div
              className="label
            "
            >
              Es sind keine offenen Anfragen vorhanden
            </div>
          )}
        </div>
        <h3 className="self-start">Bearbeitete Anfragen</h3>
        <div className="flex flex-col gap-5 pr-5 h-96 overflow-y-auto scrollbar-thin scrollbar-thumb-darkViolet scrollbar-track-transparent">
          {isLoading ? (
            <div>
              <Loader />
            </div>
          ) : sortedReviewedFixDeskRequests.length > 0 ? (
            sortedReviewedFixDeskRequests.map((fixDeskRequest: FixDeskType) => (
              <AdminFixdeskRequestsCard
                key={fixDeskRequest.id}
                fixDeskRequest={fixDeskRequest}
                setMessage={setMessage}
                setErrorMessage={setErrorMessage}
              />
            ))
          ) : (
            <div>Es sind keine bearbeiteten Anfragen vorhanden</div>
          )}
        </div>
        <div>
          <Message message={message} />
          <ErrorMessage message={errorMessage} />
        </div>
      </div>
      <BackButton />
    </div>
  );
};
