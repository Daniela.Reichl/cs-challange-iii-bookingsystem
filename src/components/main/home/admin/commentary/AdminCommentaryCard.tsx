import { useState } from "react";
import { ErrorMessage } from "../../../../common/ErrorMessage";
import { useDeleteCommentAdmin } from "../../../../../hooks/delete/useDeleteCommentAdmin";
import { Popup } from "../../../../common/Popup";
import { AdminCommentaryCardProps } from "../../../../../types/commentType";
import deleteIcon from "../../../../../assets/icons/delete.svg";

export const AdminCommentaryCard = ({
  commentary,
}: AdminCommentaryCardProps) => {
  const [errorMessage, setErrorMessage] = useState<string>("");

  const [showDeletePopup, setShowDeletePopup] = useState(false);

  const deleteComment = useDeleteCommentAdmin();

  //date
  const dateString = commentary.commentedAt;
  const date = new Date(dateString);

  // format the date to german type
  const formattedDate = date.toLocaleDateString("de-DE", {
    day: "2-digit",
    month: "2-digit",
    year: "numeric",
  });

  // handle the delete funktion
  const handleDeleteDesk = async () => {
    try {
      await deleteComment.mutateAsync(commentary.id);
      handlePopupClose();
    } catch (error) {
      if (error === 400) {
        // 	Input data invalid
        setErrorMessage("Eingabedaten ungültig");
      } else if (error === 403) {
        // The user associated with the userId in the token you sent does not have permission to access this route.
        setErrorMessage("Sie sind dafür nicht berechtigt!");
      } else {
        setErrorMessage(
          "Es ist ein Fehler aufgetreten. Versuche es später erneut."
        );
      }
    }
  };

  // open popup onlick
  const handlePopupClose = () => {
    setShowDeletePopup(false);
  };

  return (
    <div className="AdminBaseGrid flex flex-col gap-2 md:w-96 ">
      <div className="flex justify-between items-center w-full">
        {/* Desk label */}
        <h4 className="capitalize">{commentary.desk.label}</h4>
        {/* Delete button */}
        <button
          className="icon-button"
          onClick={() => setShowDeletePopup(true)}
        >
          {/* Delete icon */}
          <img
            src={deleteIcon}
            alt="Delete-Icon"
            title="Delete-Icon"
            className="w-6"
          />
        </button>
      </div>
      {/* User name */}
      <div className="flex gap-5">
        <p className="label">Name</p>
        <p className="capitalize">
          {commentary.user.firstname} {commentary.user.lastname}
        </p>
      </div>
      {/* Date */}
      <div className="flex gap-5">
        <p className="label">Datum</p>
        <p>{formattedDate}</p>
      </div>
      {/* Comment */}
      <div className="flex flex-col gap-1 col-span-2">
        <p className="label">Kommentar</p>
        <span>{commentary.comment}</span>
      </div>
      {/* ErrorMessage */}
      <ErrorMessage message={errorMessage} />
      {/* Delete popup */}
      {showDeletePopup && (
        <Popup
          message={`Soll dieser Kommentar wirklich gelöscht werden?`}
          handleSaveClick={handleDeleteDesk}
          onClose={handlePopupClose}
        />
      )}
    </div>
  );
};
