import { useState } from "react";
import { AdminCommentaryCard } from "./AdminCommentaryCard";
import { ErrorMessage } from "../../../../common/ErrorMessage";
import { Loader } from "../../../../common/Loader";
import { useGetAllCommentarys } from "../../../../../hooks/get/useGetAllCommentary";
import { CommentaryType } from "../../../../../types/commentType";
import { BackButton } from "../../../../common/BackButton";
import back from "/src/assets/icons/back.svg";
import forward from "/src/assets/icons/forward.svg";

export const AdminCommentaryview = () => {
  const [currentPage, setCurrentPage] = useState<number>(0);
  const { data, isLoading, isError } = useGetAllCommentarys(currentPage);

  if (isLoading) {
    return <Loader />;
  }
  if (isError) {
    return (
      <ErrorMessage message="Es ist ein Fehler passiert, versuche es später erneut." />
    );
  }

  // if the button > is clicked, go to next page
  const handleNextPage = () => {
    setCurrentPage(currentPage + 1);
  };

  // if the button < is clicked, go to previous page
  const handlePreviousPage = () => {
    setCurrentPage(currentPage - 1);
  };

  return (
    <div className="baseMain">
      <div className="baseContent">
        <h2>Kommentare</h2>
        <div className="grid grid-cols-1 lg:grid-cols-2 gap-6">
          {/* check if there are any comments, if not show text */}
          {data.length > 0 ? (
            data.map((commentary: CommentaryType) => (
              <AdminCommentaryCard
                key={commentary.id}
                commentary={commentary}
              />
            ))
          ) : (
            <p>Es sind keine Kommentare vorhanden</p>
          )}
        </div>
        <div className="flex gap-5 items-center ">
          {/* this is the back button */}
          <button
            className="icon-button"
            onClick={handlePreviousPage}
            disabled={currentPage === 0}
          >
            <img className="icons" src={back} alt="back" />
          </button>
          <p className="label">Seite {currentPage + 1}</p>
          {/* this is the next button */}
          <button
            className="icon-button"
            onClick={handleNextPage}
            disabled={data.length === 0}
          >
            <img className="icons" src={forward} alt="forward" />
          </button>
        </div>
      </div>
      <BackButton />
    </div>
  );
};
