import { useState } from "react";
import { useGetAllUsers } from "../../../../../hooks/get/useGetAllUsers";
import { UserType } from "../../../../../types/userType";
import { BackButton } from "../../../../common/BackButton";
import { AdminUserCard } from "./AdminUserCard";
import { Loader } from "../../../../common/Loader";
import { ErrorMessage } from "../../../../common/ErrorMessage";
import { Message } from "../../../../common/Message";
import { useGetLoggedInUser } from "../../../../../hooks/get/useGetLoggedInUser";

export const AdminUsersPageView = () => {
  const {
    data: allUsersData,
    isLoading: allUsersLoading,
    isError: allUsersError,
  } = useGetAllUsers();
  const {
    data: loggedInUser,
    isLoading: loggedInUserLoading,
    isError: loggedInUserError,
  } = useGetLoggedInUser();

  const [errorMessage, setErrorMessage] = useState<string>("");
  const [message, setMessage] = useState<string>("");

  if (allUsersLoading || loggedInUserLoading) {
    return (
      <div>
        <Loader />
      </div>
    );
  }

  if (allUsersError || loggedInUserError) {
    return (
      <div>
        Es ist ein Fehler aufgetreten, bitte versuche es später noch einmal
      </div>
    );
  }

  // Sort users by firstname in alphabetical order
  allUsersData.sort((a: UserType, b: UserType) =>
    a.firstname.localeCompare(b.firstname)
  );

  return (
    <div className="baseMain">
      {loggedInUser.isAdmin ? (
        <div className="baseContent">
          <h2>User verwalten</h2>
          <table className="table-fixed">
            <thead>
              <tr className="hidden lg:flex justify-between gap-5 font-bold text-darkViolet pb-5 px-7 text-left ">
                <th className="w-40 ">Name</th>
                <th className="w-60">E-Mail</th>
                <th className="w-40 ">Firma</th>
                <th className="w-32 ">Rolle</th>
                <th className="w-60 ">Befördern/Herabstufen</th>
                <th className="w-20 ">Löschen</th>
              </tr>
            </thead>
            <tbody className="grid grid-cols-1 gap-5 text-left pr-5 h-[30rem] overflow-y-auto scrollbar-thin scrollbar-thumb-darkViolet scrollbar-track-transparent">
              {allUsersData.map((user: UserType) => (
                <AdminUserCard
                  key={user.id}
                  user={user}
                  setErrorMessage={setErrorMessage}
                  setMessage={setMessage}
                />
              ))}
            </tbody>
          </table>
          <div>
            <ErrorMessage message={errorMessage} />
            <Message message={message} />
          </div>
        </div>
      ) : (
        <div className="baseContent">
          <h2>Du hast keine Berechtigung für diese Seite</h2>
        </div>
      )}
      <BackButton />
    </div>
  );
};
