import { useState } from "react";
import { usePromoteUser } from "../../../../../hooks/put/usePromoteUser";
import { useDeleteUser } from "../../../../../hooks/delete/useDeleteUser";
import { useDemoteUser } from "../../../../../hooks/put/useDemoteUser";
import deleteIcon from "/src/assets/icons/delete.svg";
import { Popup } from "../../../../common/Popup";
import { AdminUserCardProps } from "../../../../../types/userType";

export const AdminUserCard = ({
  user,
  setMessage,
  setErrorMessage,
}: AdminUserCardProps) => {
  const promoteUser = usePromoteUser();
  const demoteUser = useDemoteUser();

  const deleteUser = useDeleteUser();
  const [showPopup, setShowPopup] = useState(false);
  const [showUserPromotePopup, setShowUserPromotePopup] = useState(false);
  const [showUserDemotePopup, setShowUserDemotePopup] = useState(false);

  // promote User
  const handlePromoteUser = async () => {
    try {
      await promoteUser.mutateAsync(user);
      handlePopupClose();
      setMessage("User wurde zum Admin befördert!");
    } catch (error) {
      if (error === 401) {
        // Authentication not valid.
        setErrorMessage("Du bist dafür nicht berechtigt!");
      } else if (error === 403) {
        //	The user associated with the userId in the token you sent does not have permission to access this route.
        setErrorMessage("Du bist dafür nicht berechtigt!");
      }
      setErrorMessage("Es ist was schief gelaufen");
      throw new Error("An error occurred while promoting the user.");
    }
  };

  // demote User
  const handleDemotionUser = async () => {
    try {
      await demoteUser.mutateAsync(user);
      setMessage("User wurde zurückgestuft!");
      handlePopupClose();
    } catch (error) {
      if (error === 401) {
        // Authentication not valid.
        setErrorMessage("Du bist dafür nicht berechtigt!");
      } else if (error === 403) {
        //	The user associated with the userId in the token you sent does not have permission to access this route.
        setErrorMessage("Du bist dafür nicht berechtigt!");
      }
      setErrorMessage("Es ist was schief gelaufen");
      throw new Error("An error occurred while promoting the user.");
    }
  };

  //delete user
  const handleDeleteUser = async () => {
    try {
      await deleteUser.mutateAsync(user.id);
      setMessage("User wurde gelöscht!");
      handlePopupClose();
    } catch (error) {
      if (error === 401) {
        // Authentication not valid.
        setErrorMessage("Du bist dafür nicht berechtigt!");
      } else if (error === 403) {
        //	The user associated with the userId in the token you sent does not have permission to access this route.
        setErrorMessage("Du bist dafür nicht berechtigt!");
      }
      setErrorMessage("Es ist was schief gelaufen");
      throw new Error("An error occurred while promoting the user.");
    }
  };

  //close Popup
  const handlePopupClose = () => {
    setShowPopup(false);
    setShowUserDemotePopup(false);
    setShowUserPromotePopup(false);
  };

  // const handleUserPopupClose = () => {};

  return (
    <tr className="AdminUsercardGrid flex flex-col lg:flex-row items-center justify-center text-center lg:text-left gap-5 w-fit">
      <th className="capitalize w-40">
        {user.firstname} {user.lastname}
      </th>
      <th className="w-60">{user.email}</th>
      <th className="w-40">{user.department}</th>
      <th className="w-32">{user.isAdmin ? "Admin" : "User"}</th>
      <th className="flex gap-5 justify-center w-60">
        {user.isAdmin ? (
          <button
            // onClick={handleDemotionUser}
            className="button p-2"
            onClick={() => setShowUserDemotePopup(true)}
          >
            Herabstufen
          </button>
        ) : (
          <button
            // onClick={handlePromoteUser}
            className="button p-2"
            onClick={() => setShowUserPromotePopup(true)}
          >
            Befördern
          </button>
        )}
      </th>
      <th>
        <button className="icon-button" onClick={() => setShowPopup(true)}>
          <img src={deleteIcon} alt="delete user" className="w-6" />
        </button>
      </th>
      <th className="self-center text-center"></th>
      {showPopup && (
        <Popup
          message={`Soll der User ${user.firstname} ${user.lastname} wirklich gelöscht werden?`}
          handleSaveClick={handleDeleteUser}
          onClose={handlePopupClose}
        />
      )}
      {showUserPromotePopup && (
        <Popup
          message={`Soll der User ${user.firstname} ${user.lastname} wirklich befördert werden?`}
          handleSaveClick={handlePromoteUser}
          onClose={handlePopupClose}
        />
      )}
      {showUserDemotePopup && (
        <Popup
          message={`Soll der User ${user.firstname} ${user.lastname} wirklich herabgestuft werden?`}
          handleSaveClick={handleDemotionUser}
          onClose={handlePopupClose}
        />
      )}
    </tr>
  );
};
