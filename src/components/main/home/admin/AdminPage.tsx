import { Link } from "react-router-dom";
import { BackButton } from "../../../common/BackButton";
import { useGetAllFixDeskRequest } from "../../../../hooks/get/useGetAllFixDeskRequest";
import { FixDeskType } from "../../../../types/deskType";
import { Loader } from "../../../common/Loader";
import close from "/src/assets/icons/close.svg";
import { useGetLoggedInUser } from "../../../../hooks/get/useGetLoggedInUser";
import { useRefreshToken } from "../../../../hooks/post/useRefreshToken";

export const AdminPage = () => {
  const { data: allFixDeskRequests } = useGetAllFixDeskRequest();
  const { data: loggedInUser, isLoading, isError } = useGetLoggedInUser();

  const refreshToken = useRefreshToken();

  const handleRefresh = () => {
    refreshToken.mutate();
    window.location.reload();
    // seite wieder refreshen einbauen
  };

  // Filter the fix desk requests with status "notreviewed"
  const notReviewedFixDeskRequests = allFixDeskRequests
    ? allFixDeskRequests.filter(
        (fixDeskRequest: FixDeskType) => fixDeskRequest.status === "notreviewed"
      )
    : [];

  if (isLoading) {
    return <Loader />;
  }

  if (isError) {
    return (
      <div className="baseContent">
        <img src={close} alt="Close-Icon" title="Close-Icon" />
        <h3>Deine Sitzung ist abgelaufen</h3>
        <button onClick={handleRefresh} className="button">
          Refresh
        </button>
      </div>
    );
  }

  return (
    <div className="baseMain">
      {loggedInUser.isAdmin ? (
        <div className="baseContent">
          <h2>Admin Page</h2>
          <div className="grid grid-cols-1 gap-4">
            <Link to={`/admin/fixDesks`}>
              <div className=" adminGrid justify-center">
                <img
                  className="h-16"
                  src="src/assets/icons/desk.svg"
                  alt="desk"
                />
                <h3>Fixdesk Anfrage</h3>
                <p className="label">
                  {/* display how many fixdesk request are not Reviewed */}
                  {notReviewedFixDeskRequests.length} offene Anfragen
                </p>
              </div>
            </Link>
            <div className="grid grid-cols-2 md:grid-cols-3 gap-4">
              <Link to={`/admin/offices/`}>
                <div className=" adminGrid">
                  <img src="src/assets/icons/building.svg" alt="desk" />
                  <h3>Büros</h3>
                </div>
              </Link>
              <Link to={`/admin/commentary/`}>
                <div className=" adminGrid">
                  <img src="src/assets/icons/clipboard.svg" alt="clipboard" />
                  <h3>Kommentare</h3>
                </div>
              </Link>
              <Link to={`/admin/users/`}>
                <div className=" adminGrid">
                  <img src="src/assets/icons/user.svg" alt="user" />
                  <h3>User</h3>
                </div>
              </Link>
            </div>
          </div>
        </div>
      ) : (
        <div className="baseContent">
          <h2>Du hast keine Berechtigung für diese Seite</h2>
        </div>
      )}
      <BackButton />
    </div>
  );
};
