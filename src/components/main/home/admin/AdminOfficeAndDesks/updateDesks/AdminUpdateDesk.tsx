import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm, SubmitHandler } from "react-hook-form";
import { useUpdateDesk } from "../../../../../../hooks/put/useUpdateDesk";
import { getLoginToken } from "../../../../../../config/config";
import { useParams } from "react-router-dom";
import { ErrorMessage } from "../../../../../common/ErrorMessage";
import {
  DeskType,
  SingleDeskType,
  UpdatedDeskType,
} from "../../../../../../types/deskType";
import { EquipmentArray } from "../../../../../../types/enumType";
import { AdminUpdateDeskProps } from "../../../../../../types/deskType";

// yup validation
const schema = yup.object().shape({
  label: yup.string().max(30).required(),
  equipment: yup
    .array()
    .min(1)
    .required()
    .test(
      (value) =>
        value &&
        value.length > 0 &&
        value.every((item) => Object.values(EquipmentArray).includes(item))
    ),
});

export const AdminUpdateDesk = ({
  selectedDesk,
  handleSelectDesk,
  setShowDeskInfo,
  setErrorMessage,
  setMessage,
}: AdminUpdateDeskProps) => {
  const token = getLoginToken();
  const { id: officeId } = useParams();
  const { mutateAsync: updateDesk } = useUpdateDesk(
    selectedDesk.id!,
    token!,
    officeId!
  );

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<DeskType>({
    resolver: yupResolver(schema),
  });

  const handleOnSubmit: SubmitHandler<any> = async (data: UpdatedDeskType) => {
    try {
      // update selected desk
      const updatedDesk: SingleDeskType = {
        ...selectedDesk,
        label: data.label,
        equipment: data.equipment,
      };
      handleSelectDesk(updatedDesk);
      await updateDesk(updatedDesk);
      reset();
      setMessage("Tisch erfolgreich geändert");
      // hide desk info
      setShowDeskInfo(false);
    } catch (error) {
      if (error === 400) {
        // Input data invalid
        setErrorMessage("Eingabedaten ungültig");
      } else if (error === 403) {
        // The user associated with the userId in the token you sent does not have permission to access this route.
        setErrorMessage("Sie haben hierfür keine Berechtigung");
      } else if (error === 404) {
        //User with the given deskId not found.
        setErrorMessage("Es wurde kein Tisch mit dieser ID gefunden");
      } else if (error) {
        // Show any error
        setErrorMessage(
          "Es ist ein Fehler aufgetreten, versuche es später wieder"
        );
      }
    }
  };

  return (
    <div className="AdminFlexStyle self-start">
      <p className="label">Tisch ändern:</p>
      <form
        className="flex flex-col gap-5"
        onSubmit={handleSubmit(handleOnSubmit)}
      >
        <input
          className="input"
          type="text"
          placeholder={selectedDesk.label}
          {...register("label")}
        />
        {/* Desk label error message */}
        {errors.label && (
          <div>
            <ErrorMessage message={"Bitte gib dem Tisch einen neuen Namen."} />
          </div>
        )}
        <div className="flex flex-col gap-2">
          {Object.values(EquipmentArray).map((equipmentValue) => (
            <label className="flex gap-3" key={equipmentValue}>
              <input
                type="checkbox"
                value={equipmentValue}
                {...register("equipment")}
              />
              <p>{equipmentValue}</p>
            </label>
          ))}
          {/* Equipment error message */}
          {errors.equipment && (
            <div>
              <ErrorMessage
                message={"Bitte wähle mindestens eine Ausstattung aus."}
              />
            </div>
          )}
        </div>
        <button className="button" type="submit">
          Speichern
        </button>
      </form>
    </div>
  );
};
