import { useState } from "react";
import { SingleDeskType } from "../../../../../../types/deskType";
import { useGetOfficeById } from "../../../../../../hooks/get/useGetOfficeById";
import { useParams } from "react-router-dom";
import { AdminDeleteDesk } from "./AdminDeleteDesk";
import { useQueryClient } from "react-query";
import { AdminShowAllDesksProps } from "../../../../../../types/deskType";
import { Message } from "../../../../../common/Message";

export const AdminShowAllDesks = ({
  handleSelectDesk,
  showDeskInfo,
  setShowDeskInfo,
  setErrorMessage,
  setMessage,
}: AdminShowAllDesksProps) => {
  const queryClient = useQueryClient();

  const { id: officeId } = useParams();
  const showAllDesks = useGetOfficeById(officeId!);

  const [selectedDesk, setSelectedDesk] = useState<SingleDeskType | null>(null);

  // open info-field and update field of selected desk
  const handleSelectDeskLocal = (desk: SingleDeskType) => {
    setSelectedDesk(desk);
    handleSelectDesk(desk);
    queryClient.invalidateQueries("useGetOfficeById");
    setShowDeskInfo(true);
  };

  // Sort the desks alphabetically by label
  const sortedDesks = showAllDesks.data?.desks.sort(
    (a: SingleDeskType, b: SingleDeskType) => a.label.localeCompare(b.label)
  );

  return (
    <div className="AdminFlexStyle self-start">
      <p className="label">Wähle einen Tisch aus</p>
      {sortedDesks.length > 0 ? (
        <div className="grid grid-cols-2 md:grid-cols-4 gap-5">
          {sortedDesks.map((desk: SingleDeskType) => (
            <button
              key={desk.id}
              className="button"
              onClick={() => handleSelectDeskLocal(desk)}
            >
              {desk.label}
            </button>
          ))}
        </div>
      ) : (
        <div>
          <Message message={"In diesem Büro sind keine Tische verfügbar."} />
        </div>
      )}
      {selectedDesk && showDeskInfo && (
        <div className="flex flex-col gap-5 self-start">
          <p className="label">
            Tisch:{" "}
            <span className="text-darkViolet font-bold">
              {selectedDesk.label}
            </span>
          </p>
          <div className="flex flex-col gap-2 justify-center">
            <p>
              Ausstattung:{" "}
              <span className="text-darkViolet font-bold">
                {selectedDesk.equipment.join(", ")}
              </span>
            </p>
          </div>
          {selectedDesk && (
            <AdminDeleteDesk
              setErrorMessage={setErrorMessage}
              setMessage={setMessage}
              selectedDesk={selectedDesk}
              setShowDeskInfo={setShowDeskInfo}
            />
          )}
        </div>
      )}
    </div>
  );
};
