import { useState } from "react";
import { AdminShowAllDesks } from "./AdminShowAllDesks";
import { AdminUpdateDesk } from "./AdminUpdateDesk";
import { SingleDeskType } from "../../../../../../types/deskType";
import { useParams } from "react-router-dom";
import { BackButton } from "../../../../../common/BackButton";
import { useGetOfficeById } from "../../../../../../hooks/get/useGetOfficeById";
import { ErrorMessage } from "../../../../../common/ErrorMessage";
import { Message } from "../../../../../common/Message";

export const AdminUpdateDeskView = () => {
  const { id: officeId } = useParams();
  const { data: officeData } = useGetOfficeById(officeId!);

  // messages
  const [message, setMessage] = useState<string>("");
  const [errorMessage, setErrorMessage] = useState<string>("");

  // single deks
  const [deskData, setDeskData] = useState<SingleDeskType>();

  const [showDeskInfo, setShowDeskInfo] = useState<boolean>(true);

  // choose selected desk
  const handleSelectDesk = (selectedDesk: SingleDeskType) => {
    setDeskData(selectedDesk);
  };

  return (
    <div className="baseMain">
      {officeData ? (
        <div className="baseContent">
          <h2>Tische bearbeiten</h2>
          <h3>{officeData.name}</h3>
          <AdminShowAllDesks
            handleSelectDesk={handleSelectDesk}
            showDeskInfo={showDeskInfo}
            setShowDeskInfo={setShowDeskInfo}
            setMessage={setMessage}
            setErrorMessage={setErrorMessage}
          />
          {deskData && (
            <AdminUpdateDesk
              selectedDesk={deskData}
              handleSelectDesk={handleSelectDesk}
              setShowDeskInfo={setShowDeskInfo}
              setMessage={setMessage}
              setErrorMessage={setErrorMessage}
            />
          )}
          <div>
            <ErrorMessage message={errorMessage} />
            <Message message={message} />
          </div>
        </div>
      ) : null}
      <BackButton />
    </div>
  );
};
