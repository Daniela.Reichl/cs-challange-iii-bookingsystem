import { useState } from "react";
import { useDeleteDesk } from "../../../../../../hooks/delete/useDeleteDesk";
import { Popup } from "../../../../../common/Popup";
import { AdminDeleteDeskProps } from "../../../../../../types/deskType";

export const AdminDeleteDesk = ({
  selectedDesk,
  setErrorMessage,
  setMessage,
  setShowDeskInfo,
}: AdminDeleteDeskProps) => {
  const [showDeletePopup, setShowDeletePopup] = useState(false);
  const deleteDesk = useDeleteDesk();

  const handleDeleteDesk = async () => {
    try {
      // delete desk
      await deleteDesk.mutateAsync(selectedDesk.id);
      setMessage("Tisch erfolgreich gelöscht");
      handlePopupClose();
      setShowDeskInfo(false);
    } catch (error) {
      if (error === 400) {
        // 	Input data invalid
        setErrorMessage("Eingabedaten ungültig");
      } else if (error === 403) {
        //The user associated with the userId in the token you sent does not have permission to access this route.
        setErrorMessage("Sie haben hierfür keine Berechtigung");
      } else if (error) {
        // show any error
        setErrorMessage(
          "Es ist ein Fehler aufgetreten, versuche es später wieder"
        );
      }
    }
  };

  //close Popup
  const handlePopupClose = () => {
    setShowDeletePopup(false);
  };

  return (
    <div className="flex flex-col gap-5 w-fit">
      <button className="buttonFit" onClick={() => setShowDeletePopup(true)}>
        Tisch löschen
      </button>
      <div></div>
      {showDeletePopup && (
        <Popup
          message={`Soll ${selectedDesk.label} wirklich gelöscht werden?`}
          handleSaveClick={handleDeleteDesk}
          onClose={handlePopupClose}
        />
      )}
    </div>
  );
};
