import { useState } from "react";
import { Popup } from "../../../../../common/Popup";
import { useDeleteOffice } from "../../../../../../hooks/delete/useDeleteOffice";
import { OfficeProps } from "../../../../../../types/officeType";
import { useNavigate } from "react-router-dom";

type AdminOfficeDeleteProps = {
  officeData: OfficeProps;
  setErrorMessage: (value: string) => void;
};

export const AdminDeleteOffice = ({
  officeData,
  setErrorMessage,
}: AdminOfficeDeleteProps) => {
  const deleteOffice = useDeleteOffice();
  const navigate = useNavigate();
  const [showDeletePopup, setShowDeletePopup] = useState(false);

  // popup if office should be deletet
  const handleDeleteOffice = async () => {
    try {
      await deleteOffice.mutateAsync(officeData.id);

      alert("Büro wurde erfolgreich gelöscht");
      navigate(-1);
    } catch (error) {
      if (error) {
        if (error === 400) {
          // 	Input data invalid
          setErrorMessage("Eingabedaten ungültig");
          // 	The user associated with the userId in the token you sent does not have permission to access this route.
        } else if (error === 403) {
          setErrorMessage("Sie haben hierfür keine Berechtigung");
        } else {
          // show any error
          setErrorMessage(
            "Es ist ein Fehler aufgetreten, versuche es später wieder"
          );
        }
      }
    }
  };

  // open popup
  const handlePopupClose = () => {
    setShowDeletePopup(false);
  };

  return (
    <div>
      <button
        className="buttonFit w-full flex flex-col justify-center items-center"
        onClick={() => setShowDeletePopup(true)}
      >
        Büro löschen
      </button>
      {showDeletePopup && (
        <Popup
          message={`Soll ${officeData.name} wirklich gelöscht werden?`}
          handleSaveClick={handleDeleteOffice}
          onClose={handlePopupClose}
        />
      )}
    </div>
  );
};
