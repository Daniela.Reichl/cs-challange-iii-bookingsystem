import { AdminUpdateOffice } from "./AdminUpdateOffice";
import { useGetOfficeById } from "../../../../../../hooks/get/useGetOfficeById";
import { BackButton } from "../../../../../common/BackButton";
import { AdminDeleteOffice } from "./AdminDeleteOffice";
import { useParams } from "react-router-dom";
import { useState } from "react";
import { ErrorMessage } from "../../../../../common/ErrorMessage";
import { Message } from "../../../../../common/Message";

export const AdminUpdateOfficeView = () => {
  const { id: officeId } = useParams();
  const { data: officeData } = useGetOfficeById(officeId!);

  const [errorMessage, setErrorMessage] = useState<string>("");
  const [message, setMessage] = useState<string>("");

  return (
    <div className="baseMain">
      {officeData ? (
        <div className="baseContent">
          <h2>{officeData.name}</h2>
          <div className="grid grid-cols-1 gap-10">
            <AdminUpdateOffice
              setErrorMessage={setErrorMessage}
              setMessage={setMessage}
            />
            <AdminDeleteOffice
              officeData={officeData}
              setErrorMessage={setErrorMessage}
            />
          </div>
          <ErrorMessage message={errorMessage} />
          <Message message={message} />
        </div>
      ) : null}
      <BackButton />
    </div>
  );
};
