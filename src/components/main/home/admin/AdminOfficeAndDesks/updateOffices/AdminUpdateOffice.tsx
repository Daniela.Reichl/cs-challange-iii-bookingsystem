import { useUpdateOffice } from "../../../../../../hooks/put/useUpdateOffice";
import { useParams } from "react-router-dom";
import { useGetOfficeById } from "../../../../../../hooks/get/useGetOfficeById";
import { OfficeForm } from "../../../../../../types/officeType";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { getLoginToken } from "../../../../../../config/config";
import { Loader } from "../../../../../common/Loader";
import { ErrorMessage } from "../../../../../common/ErrorMessage";

type AdminUpdateOfficeProps = {
  setErrorMessage: (value: string) => void;
  setMessage: (value: string) => void;
};

// yup validation
const schema = yup.object().shape({
  name: yup.string().max(50, "Name ist zu lang").required(),
  columns: yup.number().min(1).max(10).required(),
  rows: yup.number().min(1).max(10).required(),
});

export const AdminUpdateOffice = ({
  setErrorMessage,
  setMessage,
}: AdminUpdateOfficeProps) => {
  const token = getLoginToken();
  const { id } = useParams();
  const { data: officeData } = useGetOfficeById(id!);
  const updateOfficeMutation = useUpdateOffice(id || "", token!);

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<OfficeForm>({
    resolver: yupResolver(schema),
  });

  const onSubmitHandler = async (newOfficeData: OfficeForm) => {
    try {
      const updatedData = await updateOfficeMutation.mutateAsync(newOfficeData);
      setMessage("Bürolayout erfolgreich geändert!");
      reset();
      return updatedData;
    } catch (error) {
      if (error) {
        if (error === 400) {
          // 	Input data invalid
          setErrorMessage("Eingabedaten ungültig");
        } else if (error === 403) {
          // The user associated with the userId in the token you sent does not have permission to access this route.
          setErrorMessage("Sie sind dafür nicht berechtigt!");
        } else if (error === 404) {
          // 	Desk with the given deskId (i think api means office) not found.
          setErrorMessage("Büro nicht gefunden");
        } else {
          // show any error
          setErrorMessage(
            "Es ist ein Fehler aufgetreten, versuche es später wieder"
          );
        }
      }
    }
  };

  return (
    <div className="flex flex-col gap-10 w-[24rem]">
      {officeData ? (
        <div className="AdminFlexStyle">
          <h3 className="flex gap-5">Aktuelles Bürolayout</h3>
          <div className="flex gap-10 ">
            <p>
              Spalten:{" "}
              <span className="text-darkViolet font-bold">
                {officeData.columns}
              </span>
            </p>
            <p>
              Reihen:{" "}
              <span className="text-darkViolet font-bold">
                {officeData.rows}
              </span>
            </p>
          </div>
        </div>
      ) : (
        <Loader />
      )}
      <div className="AdminFlexStyle">
        <h3>Bürolayout ändern:</h3>
        <form
          className="flex flex-col gap-2"
          onSubmit={handleSubmit(onSubmitHandler)}
        >
          <input
            className="input"
            type="text"
            placeholder={officeData.name}
            {...register("name")}
          />
          {/* Office name error message */}
          {errors.name && (
            <div>
              <ErrorMessage
                message={"Bitte gib den alten oder einen neuen Büronamen ein."}
              />
            </div>
          )}
          <input
            className="inputSmall"
            type="number"
            placeholder="Spalten"
            {...register("columns")}
          />
          {/* Columns error message */}
          {errors.columns && (
            <div>
              <ErrorMessage message={"Gib eine Zahl zwischen 1 und 10 ein"} />
            </div>
          )}
          <input
            className="inputSmall"
            type="number"
            placeholder="Reihen"
            {...register("rows")}
          />
          {/* Columns error message */}
          {errors.rows && (
            <div>
              <ErrorMessage message={"Gib eine Zahl zwischen 1 und 10 ein"} />
            </div>
          )}
          <button className="button" type="submit">
            Speichern
          </button>
        </form>
      </div>
    </div>
  );
};
