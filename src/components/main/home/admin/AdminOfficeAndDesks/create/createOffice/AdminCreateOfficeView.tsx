import { useState } from "react";
import { ErrorMessage } from "../../../../../../common/ErrorMessage";
import { BackButton } from "../../../../../../common/BackButton";
import { Message } from "../../../../../../common/Message";
import { AdminCreateOfficeInput } from "./AdminCreateOfficeInput";

export const AdminCreateOffice = () => {
  const [message, setMessage] = useState<string>("");
  const [errorMessage, setErrorMessage] = useState<string>("");

  return (
    <div className="baseMain">
      <div className="baseContent">
        <h2>Lege ein neues Büro an</h2>
        <AdminCreateOfficeInput
          setErrorMessage={setErrorMessage}
          setMessage={setMessage}
        />
        <div>
          <ErrorMessage message={errorMessage} />
          <Message message={message} />
        </div>
      </div>
      <BackButton />
    </div>
  );
};
