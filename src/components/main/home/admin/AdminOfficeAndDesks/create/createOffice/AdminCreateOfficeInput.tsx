import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { OfficeForm } from "../../../../../../../types/officeType";
import { useCreateOffice } from "../../../../../../../hooks/post/useCreateOffice";
import { ErrorMessage } from "../../../../../../common/ErrorMessage";

type AdminCreateOfficeInputProps = {
  setErrorMessage: (value: string) => void;
  setMessage: (value: string) => void;
};

// yup validation
const schema = yup.object().shape({
  name: yup.string().required(),
  columns: yup.number().min(2).max(5).required(),
  rows: yup.number().min(2).max(10).required(),
});

export const AdminCreateOfficeInput = ({
  setErrorMessage,
  setMessage,
}: AdminCreateOfficeInputProps) => {
  const createOffice = useCreateOffice();

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<OfficeForm>({
    resolver: yupResolver(schema),
  });

  const Submit = async (data: OfficeForm) => {
    try {
      //create a new office
      await createOffice.mutateAsync(data);
      setMessage("Das neue Büro wurde erfolgreich angelegt!");
      reset();
    } catch (error) {
      if (error === 400) {
        // 	Input data invalid
        setErrorMessage("Eingabedaten ungültig");
      } else if (error === 403) {
        //The user associated with the userId in the token you sent does not have permission to access this route.
        setErrorMessage("Sie haben hierfür keine Berechtigung");
      } else if (error) {
        // show any error
        setErrorMessage(
          "Es ist ein Fehler aufgetreten, versuche es später wieder"
        );
      }
    }
  };
  return (
    <form onSubmit={handleSubmit(Submit)} className="flex flex-col gap-5">
      <input
        className="input"
        type="text"
        placeholder="Namen"
        {...register("name")}
      />
      {/* Office name error message */}
      {errors.name && (
        <div>
          <ErrorMessage message={"Bitte gib einen Büronamen ein."} />
        </div>
      )}
      <input
        className="input"
        type="number"
        placeholder="Spalten"
        {...register("columns")}
      />
      {/* Columns error message */}
      {errors.columns && (
        <div>
          <ErrorMessage message={"Gib eine Zahl zwischen 2 und 5 ein."} />
        </div>
      )}
      <input
        className="input"
        type="number"
        placeholder="Reihen"
        {...register("rows")}
      />
      {/* Row error message */}
      {errors.rows && (
        <div>
          <ErrorMessage message={"Gib eine Zahl zwischen 2 und 10 ein"} />
        </div>
      )}
      <button type="submit" className="button self-center">
        Erstellen
      </button>
    </form>
  );
};
