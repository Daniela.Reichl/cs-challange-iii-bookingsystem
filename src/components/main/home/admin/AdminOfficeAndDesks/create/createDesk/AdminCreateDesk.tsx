import { useState } from "react";
import { useParams } from "react-router-dom";
import { ErrorMessage } from "../../../../../../common/ErrorMessage";
import { useGetOfficeById } from "../../../../../../../hooks/get/useGetOfficeById";
import { useCreateNewDesk } from "../../../../../../../hooks/post/useCreateNewDesk";
import { BackButton } from "../../../../../../common/BackButton";
import { Message } from "../../../../../../common/Message";
import { Loader } from "../../../../../../common/Loader";
import { AdminCreateDeskInput } from "./AdminCreateDeskInput";

export const AdminCreateDesk = () => {
  const [message, setMessage] = useState<string>("");
  const [errorMessage, setErrorMessage] = useState<string>("");

  const { id: officeId } = useParams();
  const { data: officeData } = useGetOfficeById(officeId!);
  const createDesk = useCreateNewDesk(officeId!);

  return (
    <div className="baseMain">
      <div className="baseContent">
        <div className="flex flex-col gap-10 items-center">
          <h2>Erstelle einen neuen Tisch</h2>
          {officeData ? (
            <h3> {officeData.name}</h3>
          ) : (
            <div>
              <Loader />
            </div>
          )}
          <AdminCreateDeskInput
            setErrorMessage={setErrorMessage}
            setMessage={setMessage}
            createDesk={createDesk}
          />
          <div>
            <Message message={message} />
            <ErrorMessage message={errorMessage} />
          </div>
        </div>
      </div>
      <BackButton />
    </div>
  );
};
