import { SubmitHandler, useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useCreateNewDesk } from "../../../../../../../hooks/post/useCreateNewDesk";
import { EquipmentArray } from "../../../../../../../types/enumType";
import { DeskType } from "../../../../../../../types/deskType";
import { ErrorMessage } from "../../../../../../common/ErrorMessage";

type AdminCreateDeskInputProps = {
  setErrorMessage: (value: string) => void;
  setMessage: (value: string) => void;
  createDesk: ReturnType<typeof useCreateNewDesk>;
};

// yup validation
const schema = yup.object().shape({
  label: yup.string().max(30).required(),
  equipment: yup
    .array()
    .min(1)
    .required()
    .test(
      (value) =>
        value &&
        value.length > 0 &&
        value.every((item) => Object.values(EquipmentArray).includes(item))
    ),
});

export const AdminCreateDeskInput = ({
  setErrorMessage,
  setMessage,
  createDesk,
}: AdminCreateDeskInputProps) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<DeskType>({
    resolver: yupResolver(schema),
  });

  const handleOnSubmit: SubmitHandler<DeskType> = async (data: DeskType) => {
    try {
      // crate a new desk
      await createDesk.mutateAsync(data);
      setMessage("Der neue Tisch wurde erfolgreich angelegt!");
      reset();
    } catch (error: any) {
      // Input data invalid
      if (error.status === 400) {
        setErrorMessage("Falsche Eingabe");
      }
      if (error.status === 403) {
        // The user associated with the userId in the token you sent does not have permission to access this route.
        setErrorMessage("Sie haben keine Berechtiung dafür");
      }
      // Desk with the given deskId not found
      if (error.status === 404) {
        // The user associated with the userId in the token you sent does not have permission to access this route.
        setErrorMessage("Tisch nicht gefunden");
      }
      // everyting else
      console.error("Error creating office:", error);
      setErrorMessage(
        "Es ist ein Fehler aufgetreten, versuche es später wieder"
      );
    }
  };

  return (
    <form
      className="flex flex-col gap-5"
      onSubmit={handleSubmit(handleOnSubmit)}
    >
      <input
        className="input"
        type="text"
        placeholder="Tischname"
        {...register("label")}
      />
      {/* Desk label error message */}
      {errors.label && (
        <div>
          <ErrorMessage message={"Bitte gib einen Namen ein."} />
        </div>
      )}
      <div className="flex flex-col gap-2">
        {Object.values(EquipmentArray).map((equipmentValue) => (
          <label className="flex gap-3" key={equipmentValue}>
            <input
              type="checkbox"
              value={equipmentValue}
              {...register("equipment")}
            />
            <p>{equipmentValue}</p>
          </label>
        ))}
        {/* Equipment error message */}
        {errors.equipment && (
          <div>
            <ErrorMessage message={"Bitte wähle eine Ausstattung."} />
          </div>
        )}
      </div>
      <button className="button self-center" type="submit">
        Speichern
      </button>
    </form>
  );
};
