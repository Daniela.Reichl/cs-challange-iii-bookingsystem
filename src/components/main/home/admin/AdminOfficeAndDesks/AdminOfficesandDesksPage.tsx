import { Link } from "react-router-dom";
import { BackButton } from "../../../../common/BackButton";
import edit from "/src/assets/icons/edit.svg";
import add from "/src/assets/icons/add.svg";

export const AdminOfficesPage = () => {
  const linkTags = {
    updateDesks: "/admin/offices/updateDesk",
    createDesk: "/admin/offices/createDesk",
    createOffice: "/admin/offices/createOffice",
    updateOffice: "/admin/offices/update",
  };

  return (
    <div className="baseMain">
      <div className="baseContent">
        <h2>Büros und Tische</h2>
        <div className="grid grid-cols-1 sm:grid-cols-2 gap-5 sm:gap-10 label">
          {/* Use linkTag key from the mapping object */}
          <Link
            to="/book/office-overview"
            state={{ linkTags: linkTags.updateDesks }}
          >
            <div className="adminOfficeGrid">
              <img className="icons" src={edit} alt="edit desk" />
              <p>Tische verwalten</p>
            </div>
          </Link>
          <Link
            to="/book/office-overview"
            state={{ linkTags: linkTags.updateOffice }}
          >
            <div className="adminOfficeGrid">
              <img className="icons" src={edit} alt="edit office" />
              <p>Büro verwalten</p>
            </div>
          </Link>
          <Link
            to="/book/office-overview"
            state={{ linkTags: linkTags.createDesk }}
          >
            <div className="adminOfficeGrid">
              <img className="icons" src={add} alt="edit desk" />
              <p>Tische Erstellen</p>
            </div>
          </Link>
          <Link to="/admin/offices/createOffice">
            <div className="adminOfficeGrid">
              <img className="icons" src={add} alt="add office" />
              <p>Büro anlegen</p>
            </div>
          </Link>
        </div>
      </div>
      <BackButton />
    </div>
  );
};
