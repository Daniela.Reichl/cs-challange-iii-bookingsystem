import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm, SubmitHandler } from "react-hook-form";
import { useCreateComment } from "../../../../hooks/post/useCreateComment";
import {
  CommentType,
  UserCommentInputProps,
} from "../../../../types/commentType";
import sendIcon from "../../../../assets/icons/send.png";
import close from "../../../../assets/icons/close-details.svg";

// yup validation
const schema = yup.object().shape({
  comment: yup
    .string()
    .max(150, "Der Kommentar darf nicht länger als 150 Zeichen sein")
    .required("Bitte gib einen Kommentar ein"),
});

export const UserCommentInput = ({
  setShowCommentInput,
  bookedData,
  setMessage,
  setErrorMessage,
  onClose,
}: UserCommentInputProps) => {
  const createCommentMutation = useCreateComment();

  //React hook form validation
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<CommentType>({
    resolver: yupResolver(schema),
  });

  //Send comment
  const onSubmit: SubmitHandler<CommentType> = async (data) => {
    try {
      const commentData = { value: data.comment, desk: bookedData.desk.id };
      await createCommentMutation.mutateAsync(commentData);
      setMessage(
        "Dein Kommentar wurde erfolgreich an den Admin weitergeleitet!"
      );
      setShowCommentInput(false);
    } catch (error: any) {
      if (error.response) {
        //	Input data invalid
        if (error.response.status === 400) {
          setErrorMessage("Eingabe ist fehlerhaft!");
        } else if (error.response.status === 403) {
          // 	The userIds in the JWT and the request don't match.
          setErrorMessage("Du kannst diesen Kommentar nicht bearbeiten");
        } else {
          setErrorMessage(
            "Es ist ein Fehler aufgetreten, versuche es später erneut!"
          );
        }
      }
    }
  };

  return (
    //Comment form
    <form className="flex flex-col gap-2" onSubmit={handleSubmit(onSubmit)}>
      {/* Comment input */}
      <input
        className="input"
        type="text"
        placeholder="Kommentar für den Admin schreiben"
        {...register("comment")}
      />
      {/* Error no comment */}
      <p>{errors.comment?.message}</p>
      {/* Buttons */}
      <div className="flex gap-4 justify-center">
        {/* Close button */}
        <button className="icon-button" onClick={onClose}>
          <img
            src={close}
            alt="Close-Icon"
            title="Close-Icon"
            className="w-6"
          />
        </button>
        {/* Send button */}
        <button className="icon-button" type="submit">
          <img
            src={sendIcon}
            alt="Send-Icon"
            title="Send-Icon"
            className="w-6"
          />
        </button>
      </div>
    </form>
  );
};
