import { useState } from "react";
import { useGetAllUserBookings } from "../../../../hooks/get/useGetAllUserBookings";
import { useParams } from "react-router-dom";
import { UserBookingsCard } from "./UserBookingsCard";
import { BookingType } from "../../../../types/bookingType";
import { BackButton } from "../../../common/BackButton";
import { ErrorMessage } from "../../../common/ErrorMessage";
import { Message } from "../../../common/Message";
import { Loader } from "../../../common/Loader";
import { useGetAllFixDeskRequestByUser } from "../../../../hooks/get/useGetAllFixDeskRequestByUser";

export const UserBookingsView = () => {
  const [message, setMessage] = useState<string>("");
  const [errorMessage, setErrorMessage] = useState<string>("");

  const { id: userId } = useParams<string>();

  const currentDate = new Date();

  // fetch data of flex desk
  const {
    data: bookingsData,
    isLoading: bookingsLoading,
    isError: bookingsError,
  } = useGetAllUserBookings(userId!);

  // fetch data of fix desk
  const {
    data: fixBookingsData,
    isLoading: fixBookingsLoading,
    isError: fixBookingsError,
  } = useGetAllFixDeskRequestByUser(userId!);

  if (bookingsLoading || fixBookingsLoading) {
    return <Loader />;
  }

  if (bookingsError || fixBookingsError) {
    return (
      <div>Es ist ein Fehler passiert, versuchen Sie es später noch einmal</div>
    );
  }

  // filter all bookings, if the bookings date is after the current Date
  const upcomingBookings = bookingsData.filter(
    (bookedData: BookingType) => new Date(bookedData.dateEnd) >= currentDate
  );

  // filter all bookings, if the bookings date is before the current Date
  const completedBookings = bookingsData.filter(
    (bookedData: BookingType) => new Date(bookedData.dateEnd) < currentDate
  );

  return (
    <div className="baseMain">
      <div className="baseContent">
        <h2>Meine Buchungen</h2>
        <h3>Buchungsanfragen</h3>
        <div className="bookingsOverviewGrid pr-3">
          {/* show if user have active bookings, if not show text */}
          {fixBookingsData && fixBookingsData.length > 0 && (
            <div className="flex flex-col gap-5">
              {fixBookingsData.map((fixBooking: BookingType) => (
                <UserBookingsCard
                  key={fixBooking.id}
                  bookedData={fixBooking}
                  setMessage={setMessage}
                  setErrorMessage={setErrorMessage}
                />
              ))}
            </div>
          )}
          {upcomingBookings.length > 0 ? (
            upcomingBookings.map((bookedData: BookingType) => (
              <UserBookingsCard
                key={bookedData.id}
                bookedData={bookedData}
                setMessage={setMessage}
                setErrorMessage={setErrorMessage}
              />
            ))
          ) : fixBookingsData && fixBookingsData.length === 0 ? (
            <p>Es sind keine Buchungen vorhanden</p>
          ) : null}
        </div>
        <h3>Erledigte Buchungen</h3>
        <div className="bookingsOverviewGrid h-54 pr-3 overflow-y-auto scrollbar-thin scrollbar-thumb-darkViolet scrollbar-track-transparent">
          {/* show if user made a ever booking, if not show text */}
          {completedBookings.length > 0 ? (
            completedBookings.map((bookedData: BookingType) => (
              <UserBookingsCard
                key={bookedData.id}
                bookedData={bookedData}
                setMessage={setMessage}
                setErrorMessage={setErrorMessage}
              />
            ))
          ) : (
            <p>Es sind keine erledigten Buchungen vorhanden</p>
          )}
        </div>
        <Message message={message} />
        <ErrorMessage message={errorMessage} />
      </div>
      <BackButton />
    </div>
  );
};
