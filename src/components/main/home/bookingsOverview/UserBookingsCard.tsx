import { useState } from "react";
import comment from "../../../../assets/icons/comment.svg";
import { UserCommentInput } from "./UserCommentInput";
import { UserBookingsCardProps } from "../../../../types/bookingType";

export const UserBookingsCard = ({
  bookedData,
  setMessage,
  setErrorMessage,
}: UserBookingsCardProps) => {
  const [showCommentInput, setShowCommentInput] = useState<boolean>(false);

  // date format
  const formatDate = (dateString: string) => {
    const date = new Date(dateString);
    const day = date.getDate().toString().padStart(2, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const year = date.getFullYear().toString().substr(-2);
    return `${day}.${month}.${year}`;
  };

  const today = new Date();
  const isBeforeToday = new Date(bookedData.dateEnd) < today;

  const openCommentary = () => {
    setShowCommentInput(true);
  };

  const closeCommentary = () => {
    setShowCommentInput(false);
  };

  return (
    <div className="AdminBaseGrid flex flex-col gap-4 w-72 md:w-80">
      <div className="flex justify-between items-center w-full">
        <h4 className="capitalize">{bookedData.desk.label}</h4>
        {isBeforeToday && (
          <button className="icon-button" onClick={openCommentary}>
            <img className="w-6" src={comment} alt="write comment" />
          </button>
        )}
      </div>
      <div className="flex flex-col gap-2">
        <p>
          <span className="label">Buchung: </span>
          {/* If booking has no date add "fixdesk" */}
          {bookedData.dateStart && bookedData.dateEnd
            ? `${formatDate(bookedData.dateStart)} - ${formatDate(
                bookedData.dateEnd
              )}`
            : "Fix-Desk"}
        </p>
        <p className="capitalize">
          <span className="label">Büro: </span>
          {bookedData.desk.office.name}
        </p>
        <p>
          <span className="label">Fix-Desk-Anfrage: </span>
          {/* if it is a flexdesk and not a fix desk request display "Nein, da Flex-Desk" */}
          {bookedData.status ? bookedData.status : "Nein, da Flex-Desk"}
        </p>
      </div>
      {isBeforeToday && (
        <div>
          {showCommentInput && (
            <UserCommentInput
              bookedData={bookedData}
              setShowCommentInput={setShowCommentInput}
              setMessage={setMessage}
              setErrorMessage={setErrorMessage}
              onClose={closeCommentary}
            />
          )}
        </div>
      )}
    </div>
  );
};
