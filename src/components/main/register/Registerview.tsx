import { useEffect, useState } from "react";
import { ErrorMessage } from "../../common/ErrorMessage";
import { BackButton } from "../../common/BackButton";
import { RegisterInput } from "./RegisterInput";

export const Registerview = () => {
  const [errorMessage, setErrorMessage] = useState<string>("");

  // const registerUser = useRegisterUser();
  const [isRegistered, setIsRegistered] = useState<boolean>(false);

  // go to a page that tells the user that the registration was successful
  useEffect(() => {
    if (isRegistered) {
      window.location.href = "http://localhost:5173/register/success";
    }
  }, [isRegistered]);

  return (
    <div className="baseMain">
      <div className="baseContent">
        <h2>Registrieren</h2>
        <RegisterInput
          setErrorMessage={setErrorMessage}
          setIsRegistered={setIsRegistered}
        />
        <ErrorMessage message={errorMessage} />
      </div>
      <BackButton />
    </div>
  );
};
