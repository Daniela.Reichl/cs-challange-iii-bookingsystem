import { useForm, SubmitHandler } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useGetAllDepartments } from "../../../hooks/get/useGetAllDepartments";
import { useRegisterUser } from "../../../hooks/post/useRegisterUser";
import {
  RegisterInputProps,
  RegisterInputType,
} from "../../../types/registerType";
import { CompanyEnum } from "../../../types/enumType";

// Yup validate schema
const schema = yup.object().shape({
  firstname: yup
    .string()
    .min(3, "Vorname muss mindestens 3 Zeichen lang sein")
    .max(50, "Vorname darf höchstens 50 Zeichen lang sein")
    .required("Bitte gib einen Vornamen ein"),
  lastname: yup
    .string()
    .min(2, "Nachname muss mindestens 2 Zeichen lang sein")
    .max(50, "Nachname darf höchstens 50 Zeichen lang sein")
    .required("Bitte gib einen Nachnamen ein"),
  department: yup
    .string()
    .required("Bitte wähle ein Unternehmen aus")
    .oneOf(Object.values(CompanyEnum), "Bitte wähle ein Unternehmen aus"),
  email: yup
    .string()
    .email("Bitte gib eine gültige E-Mail Adresse an")
    .required("Bitte gib eine gültige E-Mail Adresse an"),
  password: yup
    .string()
    .min(8, "Passwort muss mindestens 8 Zeichen lang sein")
    .max(32, "Passwort darf höchstens 32 Zeichen lang sein")
    .required("Bitte gib ein Passwort ein"),
  passwordrepeat: yup
    .string()
    .required("Bitte wiederhole dein Passwort")
    .test(
      "password-match",
      "Die Passwörter stimmen nicht überein",
      function (value) {
        return this.parent.password === value || value === "";
      }
    ),
});

export const RegisterInput = ({
  setErrorMessage,
  setIsRegistered,
}: RegisterInputProps) => {
  const { data: departmentsData } = useGetAllDepartments();
  const registerUser = useRegisterUser();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<RegisterInputType>({
    resolver: yupResolver(schema),
  });

  const onSubmit: SubmitHandler<RegisterInputType> = async (data) => {
    try {
      // register user
      const response = await registerUser.mutateAsync(data);
      setIsRegistered(true);
      return response;
    } catch (error: any) {
      if (error.response) {
        if (error.response.status === 400) {
          setErrorMessage(
            // Input data invalid
            "Es ist ein Fehler aufgetreten, versuche es später erneut!"
          );
        } else if (error.response.status === 409) {
          // 	A user with this email already exists
          setErrorMessage("Ein User mit dieser E-Mail existiert bereits");
        } else {
          setErrorMessage(
            "Es ist ein Fehler aufgetreten, versuche es später erneut!"
          );
        }
      }
    }
  };

  return (
    <form
      className="grid grid-cols-1 md:grid-cols-2 gap-10"
      onSubmit={handleSubmit(onSubmit)}
    >
      <div>
        <input
          className="input"
          type="text"
          placeholder="Vorname"
          {...register("firstname")}
        />
        <p>{errors.firstname?.message}</p>
      </div>
      <div>
        <input
          className="input"
          type="text"
          placeholder="Nachname"
          {...register("lastname")}
        />
        <p>{errors.lastname?.message}</p>
      </div>
      <div>
        <select className="input" {...register("department")}>
          <option value="defaultValue" hidden>
            Department
          </option>
          {departmentsData &&
            departmentsData.map((departmentName: string) => (
              <option key={departmentName} value={departmentName}>
                {departmentName}
              </option>
            ))}
        </select>
        <p>{errors.department?.message}</p>
      </div>
      <div>
        <input
          className="input"
          type="email"
          placeholder="E-Mail"
          {...register("email")}
        />
        <p>{errors.email?.message}</p>
      </div>
      <div>
        <input
          className="input"
          type="password"
          placeholder="Passwort"
          {...register("password")}
        />
        <p>{errors.password?.message}</p>
      </div>
      <div>
        <input
          className="input"
          type="password"
          placeholder="Passwort wiederholen"
          {...register("passwordrepeat")}
        />
        <p>{errors.passwordrepeat?.message}</p>
      </div>
      <button
        type="submit"
        className="button col-span-1 md:col-span-2 place-self-center"
      >
        Registrien
      </button>
    </form>
  );
};
