import { Link } from "react-router-dom";

export const RegisterSuccess = () => {
  setTimeout(() => {
    window.location.href = "/";
  }, 2000);

  return (
    <div className="baseMain">
      <div className="baseContent">
        <img src="../src/assets/icons/party-horn.svg" alt="party-icon" />
        <h2>Registrierung erfolgreich</h2>
        <p>Danke für deine Registrierung!</p>
        <button className="buttonFit">
          <Link to="/"> Zurück zum Login</Link>
        </button>
      </div>
    </div>
  );
};
