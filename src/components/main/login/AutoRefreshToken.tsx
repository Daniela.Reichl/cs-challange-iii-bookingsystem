import { useEffect } from "react";
import { useRefreshToken } from "../../../hooks/post/useRefreshToken";

export const AutoRefreshToken = () => {
  //Handle refresh token
  const refreshToken = useRefreshToken();

  useEffect(() => {
    const interval = setInterval(() => {
      // Check if the refresh token exists in local storage
      if (localStorage.refreshToken) {
        // Call the function to refresh the token
        refreshToken.mutate();
      }
      // 23 hours in milliseconds
    }, 23 * 60 * 60 * 1000);

    return () => {
      // When the component unmounts, stop the interval
      clearInterval(interval);
    };
  }, []);

  return <div className="hidden" />;
};
