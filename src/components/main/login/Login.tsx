import { useEffect } from "react";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { ErrorMessage } from "../../common/ErrorMessage";
import { useLoginUser } from "../../../hooks/post/useLoginUser";
import { Link, useNavigate } from "react-router-dom";
import { getLoginToken } from "../../../config/config";
import { CookieBanner } from "./CookieBanner";
import { LoginInput } from "../../../types/loginType";

//yup validation
const schema = yup.object().shape({
  email: yup.string().email().required(),
  password: yup.string().required(),
});

export const Login = () => {
  //Check if token in local storage - navigate to home
  const navigate = useNavigate();
  useEffect(() => {
    const loginToken = getLoginToken();
    if (loginToken) {
      navigate("/home");
    }
  }, []);

  //React hook-form validation
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<LoginInput>({ resolver: yupResolver(schema) });

  //Log in user
  const { mutate: loginUser, isError } = useLoginUser();

  const onSubmit = async (formData: LoginInput) => {
    try {
      await loginUser({
        email: formData.email,
        password: formData.password,
      });
      reset();
    } catch (error) {
      throw new Error("An error occurred while fetching desk data");
    }
  };

  return (
    <div className="baseMain">
      <h1>Desk Reservation</h1>
      <div className="baseContent">
        <h2>Login</h2>
        {/* Login Form */}
        <form
          onSubmit={handleSubmit(onSubmit)}
          className="flex flex-col gap-4 items-center"
        >
          {/* Input Email */}
          <input
            type="text"
            placeholder="Email"
            className="input"
            {...register("email")}
          />
          {/* Error Message Email */}
          {errors.email && (
            <div>
              <ErrorMessage message="Bitte gib eine gültige Email Adresse ein!" />
            </div>
          )}
          {/* Input Password */}
          <input
            type="password"
            placeholder="Passwort"
            className="input"
            autoComplete="off"
            {...register("password")}
          />
          {/* Error Message Password */}
          {errors.password && (
            <div>
              <ErrorMessage message="Bitte gib ein Passwort ein!" />
            </div>
          )}
          {/* Error Message wrong email or password */}
          {isError && (
            <div>
              <ErrorMessage message="Email Adresse oder Passwort ist falsch!" />
            </div>
          )}
          {/* Login button */}
          <button type="submit" className="button mt-4">
            Login
          </button>
        </form>
        {/* Link to register page */}
        <p className="text-center">
          Du hast noch keinen Account?
          <br />
          <Link to="/register" className="link">
            Erstelle hier einen neuen Account.
          </Link>
        </p>
      </div>
      <CookieBanner />
    </div>
  );
};
