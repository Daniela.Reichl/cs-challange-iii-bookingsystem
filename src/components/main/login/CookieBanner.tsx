import CookieConsent from "react-cookie-consent";

export const CookieBanner = () => {
  return (
    <div>
      <CookieConsent
        buttonText="Verstanden"
        cookieName="LocalStorage"
        style={{ background: "#100B26", color: "#ffffff", fontSize: "16px" }}
        location="bottom"
        buttonClasses="button"
        buttonStyle={{
          background: "#9283EF",
          borderRadius: "6px",
        }}
        overlay
        expires={150}
      >
        Diese Website verwendet Cookies, um die Benutzerfreundlichkeit zu
        verbessern.
      </CookieConsent>
    </div>
  );
};
