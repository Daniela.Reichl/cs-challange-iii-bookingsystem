import { EquipmentArray } from "./enumType";
import { UserType } from "./userType";

//Desk Type
export type DeskType = {
  label: string;
  equipment: EquipmentArray[];
};

export type FixDeskType = {
  id: string;
  user: UserType;
  desk: NewDeskType;
  status: string;
  createdAt: string;
  updatedAt: string;
  updatedBy: UserType;
};

export type NewDeskType = {
  label: string;
  id: string;
  office: string;
  fixdesk: string;
  type: boolean;
  equipment: EquipmentArray[];
};

export type SingleDeskType = {
  label: string;
  id: string;
  column: number;
  row: number;
  equipment: EquipmentArray[];
};

export type UpdatedDeskType = {
  label: string;
  office: string;
  equipment: EquipmentArray[];
};

export type DeskProps = {
  column: number;
  id: string;
  isUserFavourite: boolean;
  label: string;
  row: number;
  type: string;
  nextBooking: string;
  office: {
    columns: number;
    id: string;
    map: string;
    name: string;
    rows: number;
  };
  fixdesk: {
    id: string;
    status: string;
  };
  eqipment: [];
};

export type DeskTableProps = {
  column: number;
  row: number;
};

export type DeskElementProps = {
  desk: DeskProps;
};

export type DeskBookingProps = {
  officeId: string;
};

export type DeskDetailsProps = {
  deskId: string;
  onClose: () => void;
};

export type FixDeskProps = {
  desk: string;
};

export type AdminShowAllDesksProps = {
  handleSelectDesk: (desk: SingleDeskType) => void;
  showDeskInfo: boolean;
  setShowDeskInfo: (value: boolean) => void;
  setErrorMessage: (value: string) => void;
  setMessage: (value: string) => void;
};

export type AdminDeleteDeskProps = {
  selectedDesk: SingleDeskType;
  setErrorMessage: (value: string) => void;
  setMessage: (value: string) => void;
  setShowDeskInfo: (value: boolean) => void;
};

export type AdminUpdateDeskProps = {
  selectedDesk: SingleDeskType;
  handleSelectDesk: (updatedDesk: SingleDeskType) => void;
  setShowDeskInfo: (value: boolean) => void;
  setErrorMessage: (value: string) => void;
  setMessage: (value: string) => void;
};
