// Enums
export enum CompanyEnum {
  codingSchool = "CodingSchool",
  diamir = "Diamir",
  webundSoehne = "WebundSoehne",
  darwinsLab = "DarwinsLab",
  deepDive = "DeepDive",
  tailoredApps = "TailoredApps",
}

export enum EquipmentArray {
  HDMI = "HDMI",
  USBC = "USB C",
  HeightAdjustable = "Height Adjustable",
  DOCK = "Laptop Dock",
  MONITOR = "Monitor",
  MONITOR2 = "Monitor 2",
}
