// Login Type
export type LoginInput = {
  email: string;
  password: string;
};
