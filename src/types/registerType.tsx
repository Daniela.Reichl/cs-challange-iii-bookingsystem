import { CompanyEnum } from "./enumType";

// Register Type
export type RegisterInputType = {
  firstname: string;
  lastname: string;
  department: CompanyEnum;
  email: string;
  password: string;
  passwordrepeat: string;
};

export type RegisterInputProps = {
  setErrorMessage: (errorMessage: string) => void;
  setIsRegistered: (isRegistered: boolean) => void;
};
