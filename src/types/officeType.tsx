//Office Type
export type OfficeProps = {
  columns: number;
  id: string;
  map: string;
  name: string;
  rows: number;
  createdAt: string;
  updatedAt: string;
};

export type OfficeElementProps = {
  office: OfficeProps;
  linkTag: string;
};

export type OfficeForm = {
  name: string;
  columns: number;
  rows: number;
};
