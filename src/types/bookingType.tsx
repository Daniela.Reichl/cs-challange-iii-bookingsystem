import { EquipmentArray } from "./enumType";
import { OfficeProps } from "./officeType";
import { UserType } from "./userType";

//Booking Type
export type BookingDeskProps = {
  id: string;
  label: string;
  type: boolean;
  equipment: EquipmentArray[];
  office: OfficeProps;
};

export type BookingType = {
  status: string;
  dateStart: string;
  dateEnd: string;
  id: string;
  user: UserType;
  desk: BookingDeskProps;
  bookedAt: string;
};

export type BookingProps = {
  dateStart: string;
  dateEnd: string;
};

export type BookingSortProps = {
  dateStart: string;
  dateEnd: string;
};

export type FlexDeskBookingInput = {
  dateStart: string;
  dateEnd: string;
};

export type CreateBookingProps = {
  dateStart: string;
  dateEnd: string;
  desk: string;
};

export type UserBookingsCardProps = {
  bookedData: BookingType;
  setMessage: (value: string) => void;
  setErrorMessage: (value: string) => void;
};

// Booking buttons
export type BookingButtonsProps = {
  deskId: string;
  deskLabel: string;
};
