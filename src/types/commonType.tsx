// Message Type
export type ErrorMessageProps = {
  message: string;
};

//Popup Type
export type PopupProps = {
  message: string;
  onClose: () => void;
  handleSaveClick: () => void;
};

//API Type
export type ApiError = {
  response: {
    status: number;
  };
};

//Navbar Type
export type NavbarProps = {
  showLinks: boolean;
  onClickLink: () => void;
};
