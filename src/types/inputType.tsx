//Inputfield Type
export type InputFieldProps = {
  label: string;
  name: string;
  value: string;
  editing: boolean;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
};

export type InputFieldSelectDepartmentProps = {
  label: string;
  name: string;
  value: string;
  editing: boolean;
  options: string[];
  onChange: (event: React.ChangeEvent<HTMLSelectElement>) => void;
};
