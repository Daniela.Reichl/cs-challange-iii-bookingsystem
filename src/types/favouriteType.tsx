import { DeskProps } from "./deskType";

//Favourite Type
export type FavouriteViewProps = {
  desk: {
    label: string;
    id: string;
    office: {
      name: string;
    };
    equipment: [];
  };
  id: string;
};

export type FavouriteCardProps = {
  favourite: FavouriteViewProps;
};

export type FavouriteButtonProps = {
  desk: DeskProps;
};
