import { CompanyEnum } from "./enumType";

// User Types
export type UserProfileInput = {
  firstname: string;
  lastname: string;
  department: CompanyEnum;
  email: string;
  password: string;
};

export type UserType = {
  id: string;
  firstname: string;
  lastname: string;
  email: string;
  department: CompanyEnum;
  isAdmin: boolean;
  createdAt: string;
  updatedAt: string;
};

export type UserProfilInputProps = {
  editing: boolean;
  setEditing: (editing: boolean) => void;
};

export type AdminUserCardProps = {
  user: UserType;
  setErrorMessage: (value: string) => void;
  setMessage: (value: string) => void;
};
