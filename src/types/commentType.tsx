import { BookingType } from "./bookingType";
import { SingleDeskType } from "./deskType";
import { UserType } from "./userType";

//Comments Type
export type CommentType = {
  comment: string;
};

export type CommentCreateType = {
  value: string;
  desk: string;
};

export type CommentaryType = {
  id: string;
  comment: string;
  commentedAt: string;
  updatedAt: string;
  user: UserType;
  desk: SingleDeskType;
};

export type UserCommentInputProps = {
  setShowCommentInput: (value: boolean) => void;
  bookedData: BookingType;
  setMessage: (value: string) => void;
  setErrorMessage: (value: string) => void;
  onClose: () => void;
};

export type AdminCommentaryCardProps = {
  commentary: CommentaryType;
};
