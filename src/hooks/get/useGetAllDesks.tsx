import axios from "axios";
import { useQuery } from "react-query";
import { API_URL, getLoginToken } from "../../config/config";

export const useGetAllDesks = () => {
  return useQuery({
    queryKey: ["useGetAllDesks"],
    queryFn: async () => {
      try {
        const response = await axios.get(`${API_URL}/api/desks`, {
          headers: {
            Authorization: `Bearer ${getLoginToken()}`,
            Accept: "application/json",
          },
        });
        return response.data;
      } catch (error: any) {
        if (error.status === 400) {
          // Any of the provided query parameters contains an invalid value.
          throw new Error("Invalid query parameter value");
        } else if (error.status === 401) {
          // Authentication not valid.
          throw new Error("Authentication not valid");
        } else {
          // Handle other errors
          throw new Error("An error occurred while fetching desk data");
        }
      }
    },
  });
};
