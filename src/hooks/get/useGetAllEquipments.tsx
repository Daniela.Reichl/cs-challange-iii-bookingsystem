import axios from "axios";
import { API_URL } from "../../config/config";
import { useQuery } from "react-query";

export const useGetAllEquipments = () => {
  return useQuery(["GetAllEquipments"], async () => {
    try {
      const response = await axios.get(`${API_URL}/api/equipments`);
      return response.data;
    } catch (error) {
      throw new Error("Failed to fetch equipment data");
    }
  });
};
