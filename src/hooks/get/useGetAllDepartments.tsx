import axios from "axios";
import { API_URL } from "../../config/config";
import { useQuery } from "react-query";

export const useGetAllDepartments = () => {
  return useQuery(["GetAllDepartments"], async () => {
    try {
      const response = await axios.get(`${API_URL}/api/departments`);
      // We must use Object.keys() because the API only response a object and not a list.
      // We have to make strings out of it.
      return Object.keys(response.data);
    } catch (error) {
      throw new Error("Failed to fetch department data");
    }
  });
};
