import axios from "axios";
import { useQuery } from "react-query";
import { API_URL, getLoginToken } from "../../config/config";

export const useGetAllOffices = () => {
  return useQuery({
    queryKey: ["useGetAllOffices"],
    queryFn: async () => {
      const response = await axios.get(`${API_URL}/api/offices`, {
        headers: {
          Authorization: `Bearer ${getLoginToken()}`,
          Accept: "application/json",
        },
      });
      return response.data;
    },
  });
};
