import axios from "axios";
import { useQuery } from "react-query";
import { API_URL, getLoginToken } from "../../config/config";

export const useGetAllFixDeskRequestByUser = (id: string) => {
  return useQuery({
    queryKey: ["getAllFixDeskRequestByUser"],
    queryFn: async () => {
      try {
        const response = await axios.get(
          `${API_URL}/api/fixdesk-requests/user/${id}`,
          {
            headers: {
              Authorization: `Bearer ${getLoginToken()}`,
              Accept: "application/json",
            },
          }
        );
        return response.data;
      } catch (error) {
        throw new Error("Failed to fetch fix desk requests by user");
      }
    },
  });
};
