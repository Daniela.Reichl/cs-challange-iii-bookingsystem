import axios from "axios";
import { useQuery } from "react-query";
import { API_URL, getLoginToken } from "../../config/config";

export const useGetOfficeById = (id: string) => {
  return useQuery({
    queryKey: ["useGetOfficeById", id],
    queryFn: async () => {
      const loginToken = getLoginToken();
      const response = await axios.get(`${API_URL}/api/offices/${id}`, {
        headers: {
          Authorization: `Bearer ${loginToken}`,
          Accept: "application/json",
        },
      });
      return response.data;
    },
  });
};
