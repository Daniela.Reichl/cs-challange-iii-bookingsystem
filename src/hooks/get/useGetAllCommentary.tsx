import axios from "axios";
import { API_URL, getLoginToken } from "../../config/config";
import { useQuery } from "react-query";

export const useGetAllCommentarys = (page: number) => {
  return useQuery(["GetAllComemntary", page], async () => {
    try {
      const response = await axios.get(`${API_URL}/api/comments`, {
        params: {
          page: page,
        },
        headers: {
          Authorization: `Bearer ${getLoginToken()}`,
          Accept: "application/json",
        },
      });
      return response.data;
    } catch (error) {
      throw new Error("An error occurred while fetching commentary data.");
    }
  });
};
