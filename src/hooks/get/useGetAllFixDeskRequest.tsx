import axios from "axios";
import { useQuery } from "react-query";
import { API_URL, getLoginToken } from "../../config/config";

export const useGetAllFixDeskRequest = () => {
  return useQuery({
    queryKey: ["getAllFixDeskRequest"],
    queryFn: async () => {
      const response = await axios.get(
        `${API_URL}/api/admin/fix-desk-requests`,
        {
          headers: {
            Authorization: `Bearer ${getLoginToken()}`,
            Accept: "application/json",
          },
        }
      );
      return response.data;
    },
  });
};
