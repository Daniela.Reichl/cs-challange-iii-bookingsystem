import axios from "axios";
import { useQuery } from "react-query";
import { API_URL, getLoginToken } from "../../config/config";

export const useGetLoggedInUser = () => {
  return useQuery({
    queryKey: ["useGetLoggedInUser"],
    queryFn: async () => {
      // const loginToken = localStorage.getItem("loginToken");
      const response = await axios.get(`${API_URL}/api/users/profile`, {
        headers: {
          Authorization: `Bearer ${getLoginToken()}`,
          Accept: "application/json",
        },
      });
      return response.data;
    },
  });
};
