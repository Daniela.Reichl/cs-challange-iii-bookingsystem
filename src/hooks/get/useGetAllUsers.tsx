import { useQuery } from "react-query";
import { API_URL, getLoginToken } from "../../config/config";
import axios from "axios";

export const useGetAllUsers = () => {
  return useQuery({
    queryKey: ["useGetAllUsers"],
    queryFn: async () => {
      const response = await axios.get(`${API_URL}/api/users`, {
        headers: {
          Authorization: `Bearer ${getLoginToken()}`,
          Accept: "application/json",
        },
      });
      return response.data;
    },
  });
};
