import { useNavigate } from "react-router-dom";

export const useGetToken = () => {
  try {
    const loginToken = localStorage.getItem("loginToken");
    const refreshToken = localStorage.getItem("refreshToken");

    if (loginToken) {
      return loginToken;
    } else if (refreshToken) {
      return refreshToken;
    } else {
      const navigate = useNavigate();
      navigate("/");
      return null;
    }
  } catch (error) {
    console.error("Error while getting token:", error);
    return null;
  }
};
