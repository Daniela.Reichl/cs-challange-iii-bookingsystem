import axios from "axios";
import { useQuery } from "react-query";
import { API_URL, getLoginToken } from "../../config/config";
import { SingleDeskType } from "../../types/deskType";

export const useGetAllDeskByOffice = (id: string) => {
  return useQuery<SingleDeskType[]>({
    queryKey: ["GetAllDeskByOffice"],
    queryFn: async () => {
      const response = await axios.get(`${API_URL}/api/admin/layout/${id}`, {
        headers: {
          Authorization: `Bearer ${getLoginToken()}`,
          Accept: "application/json",
        },
      });
      return response.data.desks;
    },
  });
};
