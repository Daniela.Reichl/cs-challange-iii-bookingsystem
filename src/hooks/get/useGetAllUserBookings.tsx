import axios from "axios";
import { useQuery } from "react-query";
import { API_URL, getLoginToken } from "../../config/config";

export const useGetAllUserBookings = (id: string) => {
  return useQuery({
    queryKey: ["getAllBookingsByUser", id],
    queryFn: async () => {
      const loginToken = getLoginToken();
      const response = await axios.get(`${API_URL}/api/bookings/user/${id}`, {
        headers: {
          Authorization: `Bearer ${loginToken}`,
          Accept: "application/json",
        },
      });
      return response.data;
    },
  });
};
