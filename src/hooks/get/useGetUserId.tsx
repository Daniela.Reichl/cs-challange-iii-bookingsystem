import axios from "axios";
import { API_URL } from "../../config/config";
import { useQuery } from "react-query";

export const useGetUserId = (id: string, token: string) => {
  return useQuery(["GetUserbyID"], async () => {
    const response = await axios.get(`${API_URL}/api/users/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    return response.data;
  });
};
