import axios from "axios";
import { API_URL, getLoginToken } from "../../config/config";
import { useQuery } from "react-query";

export const useGetCommentById = (id: string) => {
  return useQuery(["GetCommentById"], async () => {
    const response = await axios.get(`${API_URL}/api/comments/one/${id}`, {
      headers: {
        Authorization: `Bearer ${getLoginToken()}`,
      },
    });
    return response.data;
  });
};
