import axios from "axios";
import { API_URL, getLoginToken } from "../../config/config";
import { useMutation, useQueryClient } from "react-query";

export const useDeleteFavourite = () => {
  const queryClient = useQueryClient();
  return useMutation(
    async (favouriteId: string) => {
      const token = getLoginToken();
      const response = await axios.delete(
        `${API_URL}/api/favourites/${favouriteId}`,
        {
          headers: {
            Authorization: `Bearer ${token}`,
            accept: "application/json",
          },
        }
      );
      return response;
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries("useGetAllUserFavourites");
      },
      onError: (error) => {
        console.error("Error deleting comment:", error);
      },
    }
  );
};
