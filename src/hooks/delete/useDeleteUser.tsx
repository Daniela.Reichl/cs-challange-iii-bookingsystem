import axios from "axios";
import { API_URL, getLoginToken } from "../../config/config";
import { useMutation, useQueryClient } from "react-query";

export const useDeleteUser = () => {
  const queryClient = useQueryClient();
  return useMutation(
    async (id: string) => {
      const authToken = getLoginToken();
      const response = await axios.delete(`${API_URL}/api/admin/users/${id}`, {
        headers: {
          Authorization: `Bearer ${authToken}`,
        },
      });
      return response.data;
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries("useGetAllUsers");
      },
      onError: (error) => {
        console.error("Error deleting comment:", error);
      },
    }
  );
};
