import axios from "axios";
import { API_URL, getLoginToken } from "../../config/config";
import { useMutation, useQueryClient } from "react-query";

export const useDeleteDesk = () => {
  const queryClient = useQueryClient();
  return useMutation(
    async (deskID: string) => {
      const response = await axios.delete(
        `${API_URL}/api/admin/desks/${deskID}`,
        {
          headers: {
            Authorization: `Bearer ${getLoginToken()}`,
          },
        }
      );
      return response;
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries("useGetOfficeById");
      },
      onError: (error) => {
        console.error("Error deleting comment:", error);
      },
    }
  );
};
