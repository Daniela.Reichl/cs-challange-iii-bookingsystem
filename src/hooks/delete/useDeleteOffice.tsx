import axios from "axios";
import { API_URL, getLoginToken } from "../../config/config";
import { useMutation } from "react-query";

export const useDeleteOffice = () => {
  return useMutation(async (id: string) => {
    try {
      const authToken = getLoginToken();
      const response = await axios.delete(
        `${API_URL}/api/admin/offices/${id}`,
        {
          headers: {
            Authorization: `Bearer ${authToken}`,
          },
        }
      );
      return response.data;
    } catch (error) {
      throw error;
    }
  });
};
