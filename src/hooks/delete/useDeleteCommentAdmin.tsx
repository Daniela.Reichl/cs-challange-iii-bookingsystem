import axios from "axios";
import { API_URL, getLoginToken } from "../../config/config";
import { useMutation, useQueryClient } from "react-query";

export const useDeleteCommentAdmin = () => {
  const queryClient = useQueryClient();
  return useMutation(
    async (id: string) => {
      const response = await axios.delete(
        `${API_URL}/api/admin/comments/${id}`,
        {
          headers: {
            Authorization: `Bearer ${getLoginToken()}`,
          },
        }
      );
      return response;
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries("GetAllComemntary");
      },
      onError: (error) => {
        console.error("Error deleting comment:", error);
      },
    }
  );
};
