import axios from "axios";
import { API_URL } from "../../config/config";
import { useMutation } from "react-query";
import { RegisterInputType } from "../../types/registerType";

export const useRegisterUser = () => {
  return useMutation(async (user: RegisterInputType) => {
    const response = await axios.post(`${API_URL}/api/users/register`, {
      firstname: user.firstname,
      lastname: user.lastname,
      email: user.email,
      department: user.department,
      password: user.password,
    });
    return response.data;
  });
};
