import { useMutation } from "react-query";
import {
  API_URL,
  getRefreshToken,
  setLoginToken,
  setRefreshToken,
} from "../../config/config";
import axios from "axios";
import { useNavigate } from "react-router-dom";

export const useRefreshToken = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationFn: async () => {
      const token = getRefreshToken();
      const response = await axios.post(
        `${API_URL}/api/users/refresh`,
        {
          refresh: token,
        },
        { headers: { accept: "application/json" } }
      );
      return response.data;
    },
    onSuccess: (data) => {
      setLoginToken(data.token);
      setRefreshToken(data.refresh);
    },
    onError: (error) => {
      console.error("Error refreshing token:", error);
      //Logout user
      localStorage.removeItem("loginToken");
      localStorage.removeItem("refreshToken");
      navigate("/");
      window.location.reload();
    },
  });
};
