import axios from "axios";
import { useMutation, useQueryClient } from "react-query";
import { API_URL, getLoginToken } from "../../config/config";

import { CommentCreateType } from "../../types/commentType";

export const useCreateComment = () => {
  const queryClient = useQueryClient();
  return useMutation({
    mutationFn: async (comment: CommentCreateType) => {
      try {
        const token = getLoginToken();
        const response = await axios.post(
          `${API_URL}/api/comments`,
          {
            comment: comment.value,
            desk: comment.desk,
          },
          {
            headers: {
              Authorization: `Bearer ${token}`,
              accept: "application/json",
            },
          }
        );
        return response.data;
      } catch (error) {
        throw new Error("An error occurred while creating the comment");
      }
    },
    onSuccess: (data) => {
      queryClient.invalidateQueries("getAllBookingsByUser");
    },
  });
};
