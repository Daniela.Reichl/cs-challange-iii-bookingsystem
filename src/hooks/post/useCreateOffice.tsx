import axios from "axios";
import { API_URL, getLoginToken } from "../../config/config";
import { useMutation } from "react-query";
import { OfficeForm } from "../../types/officeType";

export const useCreateOffice = () => {
  return useMutation(async (office: OfficeForm) => {
    const token = getLoginToken();
    const response = await axios.post(
      `${API_URL}/api/admin/offices`,
      {
        name: office.name,
        columns: office.columns,
        rows: office.rows,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    return response.data;
  });
};
