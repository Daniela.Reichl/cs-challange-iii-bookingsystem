import axios from "axios";
import { useMutation } from "react-query";
import { API_URL, getLoginToken } from "../../config/config";
import { useNavigate } from "react-router-dom";
import { CreateBookingProps } from "../../types/bookingType";

export const useCreateBooking = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationFn: async (booking: CreateBookingProps) => {
      const token = getLoginToken();
      const response = await axios.post(
        `${API_URL}/api/bookings`,
        {
          dateStart: booking.dateStart,
          dateEnd: booking.dateEnd,
          desk: booking.desk,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
            accept: "application/json",
          },
        }
      );
      return response.data;
    },
    onSuccess: (data) => {
      navigate("/book/success");
    },
  });
};
