import axios from "axios";
import { useMutation } from "react-query";
import { API_URL, setLoginToken, setRefreshToken } from "../../config/config";
import { LoginInput } from "../../types/loginType";
import { useNavigate } from "react-router-dom";

export const useLoginUser = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationFn: async (user: LoginInput) => {
      const response = await axios.post(
        `${API_URL}/api/users/login`,
        {
          email: user.email,
          password: user.password,
        },
        { headers: { accept: "application/json" } }
      );
      return response.data;
    },
    onSuccess: (data) => {
      setLoginToken(data.token);
      setRefreshToken(data.refresh);
      navigate("/home");
      window.location.reload();
    },
  });
};
