import axios from "axios";
import { useMutation, useQueryClient } from "react-query";
import { API_URL, getLoginToken } from "../../config/config";
import { FixDeskProps } from "../../types/deskType";

export const useCreateFavorite = () => {
  const queryClient = useQueryClient();
  return useMutation(
    async (desk: FixDeskProps) => {
      const token = getLoginToken();
      const response = await axios.post(
        `${API_URL}/api/favourites`,
        {
          desk: desk.desk,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
            accept: "application/json",
          },
        }
      );
      return response.data;
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries("useGetAllDesks");
      },
    }
  );
};
