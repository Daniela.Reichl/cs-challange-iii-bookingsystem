import axios from "axios";
import { useMutation } from "react-query";
import { API_URL, getLoginToken } from "../../config/config";
import { useNavigate } from "react-router-dom";
import { FixDeskProps } from "../../types/deskType";

export const useCreateFixdesk = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationFn: async (desk: FixDeskProps) => {
      const token = getLoginToken();
      const response = await axios.post(
        `${API_URL}/api/fixdesk-requests`,
        {
          desk: desk.desk,
        },
        {
          headers: {
            Authorization: `Bearer ${token}`,
            accept: "application/json",
          },
        }
      );
      return response.data;
    },
    onSuccess: (data) => {
      navigate("/request/fixdesk-success");
    },
  });
};
