import axios from "axios";
import { API_URL, getLoginToken } from "../../config/config";
import { useMutation } from "react-query";
import { DeskType } from "../../types/deskType";

export const useCreateNewDesk = (officeId: string) => {
  return useMutation(async (desk: DeskType) => {
    const token = getLoginToken();
    const response = await axios.post(
      `${API_URL}/api/admin/desks`,
      {
        label: desk.label,
        equipment: desk.equipment,
        office: officeId,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    return response.data;
  });
};
