import axios from "axios";
import { API_URL, getLoginToken } from "../../config/config";
import { useMutation, useQueryClient } from "react-query";
import { UserType } from "../../types/userType";

export const usePromoteUser = () => {
  const queryClient = useQueryClient();
  return useMutation<void, unknown, UserType>(
    async (user: UserType) => {
      try {
        await axios.put(
          `${API_URL}/api/admin/promote`,
          {
            id: user.id,
            isAdmin: true,
          },
          {
            headers: {
              Authorization: `Bearer ${getLoginToken()}`,
            },
          }
        );
      } catch (error) {
        console.error("Error promoting user:", error);
        throw error;
      }
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries("useGetAllUsers");
      },
    }
  );
};
