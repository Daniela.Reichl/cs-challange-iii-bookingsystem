import axios from "axios";
import { API_URL } from "../../config/config";
import { useMutation, useQueryClient } from "react-query";
import { DeskType } from "../../types/deskType";

export const useUpdateDesk = (
  deskId: string,
  token: string,
  officeId: string
) => {
  const queryClient = useQueryClient();
  return useMutation<DeskType, unknown, DeskType>(
    async (desk: DeskType) => {
      try {
        const response = await axios.put(
          `${API_URL}/api/admin/desks/${deskId}`,
          {
            label: desk.label,
            equipment: desk.equipment,
            office: officeId,
          },
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
        return response.data;
      } catch (error) {
        console.error("Error updating desk:", error);
        throw error;
      }
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries("useGetOfficeById");
      },
    }
  );
};
