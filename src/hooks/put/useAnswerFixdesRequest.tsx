import axios from "axios";
import { API_URL, getLoginToken } from "../../config/config";
import { useMutation, useQueryClient } from "react-query";
import { FixDeskType } from "../../types/deskType";

export const useAnswerFixdeskRequest = () => {
  const queryClient = useQueryClient();
  return useMutation(
    async (fixDesk: FixDeskType) => {
      try {
        const response = await axios.put(
          `${API_URL}/api/admin/fix-desk-requests/`,
          {
            id: fixDesk.id,
            status: fixDesk.status,
          },
          {
            headers: {
              Authorization: `Bearer ${getLoginToken()}`,
            },
          }
        );
        return response.data;
      } catch (error) {
        console.error("Error updating user:", error);
        throw error;
      }
    },
    {
      onSuccess: (data) => {
        queryClient.invalidateQueries("getAllFixDeskRequest");
      },
    }
  );
};
