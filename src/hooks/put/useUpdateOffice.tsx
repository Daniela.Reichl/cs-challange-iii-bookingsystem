import axios from "axios";
import { API_URL } from "../../config/config";
import { useMutation, useQueryClient } from "react-query";
import { OfficeForm } from "../../types/officeType";

export const useUpdateOffice = (id: string, token: string) => {
  const queryClient = useQueryClient();
  return useMutation(
    async (office: OfficeForm) => {
      try {
        const response = await axios.put(
          `${API_URL}/api/admin/offices/${id}`,
          {
            name: office.name,
            columns: office.columns,
            rows: office.rows,
          },
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
        return response.data;
      } catch (error) {
        console.error("Error updating office:", error);
        throw error;
      }
    },
    {
      onSuccess: (data) => {
        queryClient.invalidateQueries("useGetOfficeById");
      },
    }
  );
};
