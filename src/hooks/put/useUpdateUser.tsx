import axios from "axios";
import { API_URL } from "../../config/config";
import { useMutation } from "react-query";
import { UserProfileInput } from "../../types/userType";

export const useUpdateUser = (id: string, token: string) => {
  return useMutation(async (user: UserProfileInput) => {
    try {
      const response = await axios.put(`${API_URL}/api/users/${id}`, user, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      return response.data;
    } catch (error) {
      console.error("Error updating user:", error);
      throw error;
    }
  });
};
