import { QueryClient, QueryClientProvider } from "react-query";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Login } from "./components/main/login/Login";
import { HomePage } from "./components/main/home/HomePage";
import { Layout } from "./components/common/Layout";
import { UserProfileView } from "./components/main/home/userprofil/UserProfileView";
import { Registerview } from "./components/main/register/Registerview";
import { RegisterSuccess } from "./components/main/register/RegisterSuccess";
import { AdminPage } from "./components/main/home/admin/AdminPage";
import { AdminCommentaryview } from "./components/main/home/admin/commentary/AdminCommentaryview";
import { AdminFixdesksRequestView } from "./components/main/home/admin/fixdeskRequests/AdminFixdesksRequestView";
import { AdminOfficesPage } from "./components/main/home/admin/AdminOfficeAndDesks/AdminOfficesandDesksPage";
import { AdminCreateOffice } from "./components/main/home/admin/AdminOfficeAndDesks/create/createOffice/AdminCreateOfficeView";
import { AdminCreateDesk } from "./components/main/home/admin/AdminOfficeAndDesks/create/createDesk/AdminCreateDesk";
import { AdminUpdateOfficeView } from "./components/main/home/admin/AdminOfficeAndDesks/updateOffices/AdminUpdateOfficeView";
import { AdminUpdateDeskView } from "./components/main/home/admin/AdminOfficeAndDesks/updateDesks/AdminUpdateDeskView";
import { AdminUsersPageView } from "./components/main/home/admin/usersPage/AdminUsersPageView";
import { UserBookingsView } from "./components/main/home/bookingsOverview/UserBookingsView";
import { FixdeskSuccess } from "./components/main/home/booking/fixdesk/FixdeskSuccess";
import { FixDeskRequestSummary } from "./components/main/home/booking/fixdesk/FixdeskRequestSummary";
import { FavouriteView } from "./components/main/home/favourites/FavouriteView";
import { GDPRPage } from "./components/main/impressumAndGDPR/GDPR";
import { Impressum } from "./components/main/impressumAndGDPR/Impressum";
import { NoPage } from "./components/common/NoPage";
import { ErrorBoundary } from "react-error-boundary";
import { ErrorBoundaryComponent } from "./components/common/ErrorBoundaryComponent";
import { OfficeOverview } from "./components/main/home/booking/OfficeOverview";
import { OfficeBookingPlan } from "./components/main/home/booking/OfficeBookingPlan";
import { DeskBookingSummary } from "./components/main/home/booking/flexdesk/DeskBookingSummary";
import { DeskBookingSuccess } from "./components/main/home/booking/flexdesk/DeskBookingSuccess";

const queryClient = new QueryClient();

export function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <BrowserRouter>
        <ErrorBoundary FallbackComponent={ErrorBoundaryComponent}>
          <Routes>
            <Route path="/" element={<Layout />}>
              {/* Login & Register User */}
              <Route index element={<Login />} />
              <Route path="/register" element={<Registerview />} />
              <Route path="/register/success" element={<RegisterSuccess />} />

              {/* Homepage */}
              <Route path="/home" element={<HomePage />} />

              {/* User Profile */}
              <Route path="/user/profile/:id" element={<UserProfileView />} />
              <Route path="/user/bookings/:id" element={<UserBookingsView />} />

              {/* Favourites Section */}
              <Route path="/favourites/:id" element={<FavouriteView />} />

              {/* Booking Section */}
              <Route
                path="/book/office-overview"
                element={<OfficeOverview />}
              />
              <Route
                path="/book/office-bookingplan/:id"
                element={<OfficeBookingPlan />}
              />
              <Route path="/book/summary" element={<DeskBookingSummary />} />
              <Route path="/book/success" element={<DeskBookingSuccess />} />
              <Route
                path="/request/summary"
                element={<FixDeskRequestSummary />}
              />
              <Route
                path="/request/fixdesk-success"
                element={<FixdeskSuccess />}
              />

              {/* Admin Section */}
              <Route path="/admin" element={<AdminPage />} />
              <Route path="/admin/offices" element={<AdminOfficesPage />} />
              <Route
                path="/admin/commentary"
                element={<AdminCommentaryview />}
              />
              <Route path="/admin/users" element={<AdminUsersPageView />} />
              <Route
                path="/admin/fixDesks"
                element={<AdminFixdesksRequestView />}
              />
              <Route
                path="/admin/offices/createOffice"
                element={<AdminCreateOffice />}
              />
              <Route
                path="/admin/offices/createDesk/:id"
                element={<AdminCreateDesk />}
              />
              <Route
                path="/admin/offices/update/:id"
                element={<AdminUpdateOfficeView />}
              />
              <Route
                path="/admin/offices/updateDesk/:id"
                element={<AdminUpdateDeskView />}
              />
              <Route
                path="admin/offices/book/office-overview"
                element={<OfficeOverview />}
              />

              {/* Footer Section */}
              <Route path="admin/gdpr-page" element={<GDPRPage />} />
              <Route path="admin/impressum" element={<Impressum />} />

              <Route path="*" element={<NoPage />} />
            </Route>
          </Routes>
        </ErrorBoundary>
      </BrowserRouter>
    </QueryClientProvider>
  );
}

export default App;
